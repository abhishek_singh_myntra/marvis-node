###############################################
## Configure the app name and the port
## number on which the app should run.
###############################################

APP_NAME = marvis-node
PORT ?= 10256
NO_OF_INSTANCE = 1
NMOD = ./node_modules
CACHE_DIR = _npmcache
PID_FILE = /myntra/pid/marvis-node-health.pid
npm = npm --registry http://registry.myntra.com:8000 --color false --spin false --cache $(CACHE_DIR)

# binaries in the node modules directory
NBIN = ./node_modules/.bin

export NODE_ENV ?= development

# Operator := resolves once, otherwise we will have a loop here
export PATH := $(NBIN):$(PATH)

#LOGDIR = /myntra/node_logs/marvis-node
# Setup path to log directory
ifeq ($(NODE_ENV), production)
	LOGDIR = logs
else
	LOGDIR = logs
	NO_OF_INSTANCE = 1
endif

###############################################
## Development related tasks.
###############################################
#@if [ -d $(CACHE_DIR) ]; then rm -rf $(CACHE_DIR); echo "removed dir: $(CACHE_DIR)"; fi;

clean:
	@if [ -d dist ]; then rm -rf dist; echo "removed dir: dist"; fi;
	@if [ -d build ]; then rm -rf build; echo "removed dir: build"; fi;

install:
	@mkdir -p $(CACHE_DIR) || echo "created npm cache dir: $(CACHE_DIR)";
	@test -d node_modules && rm -rf node_modules || echo "node_modules dir not exists";
	yarn
	tar -czf _nm.tgz node_modules

build:
	gulp build;
# gulp dist;

link:
	@for i in $$(egrep -o '"myx-.*"\s*:' package.json | sed 's/[^a-z-]//g'); do \
		if [ -d "../$$i" ]; then \
			pushd ../$$i; \
			for j in $$(egrep -o '"myx-.*"\s*:' package.json | sed 's/[^a-z-]//g'); do \
				npm link $$j; \
			done; \
			popd; \
		fi; \
		npm link $$i; \
	done	

unlink:
	@for i in $$(egrep -o '"myx-.*"\s*:' package.json | sed 's/[^a-z-]//g'); do npm unlink $$i; done	

linkcheck:
	@ls -l node_modules | grep myx-
	@for i in $$(egrep -o '"myx-.*"\s*:' package.json | sed 's/[^a-z-]//g'); do \
		echo $$i; \
		(ls -l ../$$i/node_modules | grep myx-) || echo No myx modules; \
	done	

start:
	@test -d $(LOGDIR) || (echo "ERROR: Log directory $(LOGDIR) does not exist. Trying to create..." && mkdir -p $(LOGDIR));
	pm2 build/start bot.js --node-args="--harmony" -f --name $(APP_NAME) -i $(NO_OF_INSTANCE) -e $(LOGDIR)/err.log -o $(LOGDIR)/out.log;

stop:
	pm2 delete $(APP_NAME);

restart:
	pm2 restart $(APP_NAME);

live:
	@if [ ! -f "$(PID_FILE)" ]; then (export NODE_WORKERS=2; nohup node health.js >$(LOGDIR)/health.log 2>&1 &); echo App is live; fi

dead:
	@if [ -f "$(PID_FILE)" ]; then kill -9 $$(cat $(PID_FILE)); rm $(PID_FILE); echo App is dead; fi

watch:
	gulp watch

nodemon:
	@nodemon server.js --ext js,jade \
		--watch src/routes/ \
		--watch src/lib/ \
		--watch src/services/ \
		--watch src/views/ \
		--watch ../myx-lib/ \
		--watch ../unity-middlewares/ \
		--watch ../unity-config/ \
		--watch ../myx-outline/ \
		--watch server.js

logs:
	pm2 logs $(APP_NAME);

# Python Switching Logic
# Creating a symlink to correct python version
# in a path that has been added to the PATH env
pythonfix-remove:
ifneq ($(NODE_ENV), production)
	rm -f $(NBIN)/python;
endif

pythonfix-add:
ifneq ($(NODE_ENV), production)
	@echo Not in production, adding python 2.7 to PATH;
	mkdir -p $(NBIN);
	ln -sf /usr/bin/python2.7 $(NBIN)/python;
endif

# So that makefile does not give already up-to-date errors.
.PHONY: clean install build start stop restart live dead watch nodemon logs link unlink linkcheck pythonfix-remove pythonfix-add