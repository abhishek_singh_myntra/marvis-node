'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by 12072 on 8/9/16.
 */

var Chart = require('./Chart');
var _ = require('underscore');

var Bar = function (_Chart) {
    _inherits(Bar, _Chart);

    function Bar(props) {
        _classCallCheck(this, Bar);

        var _this = _possibleConstructorReturn(this, (Bar.__proto__ || Object.getPrototypeOf(Bar)).call(this, props));

        _this.chartOptions["plotOptions"]["pie"]["dataLabels"] = { enabled: false };

        var widget = props.widget;
        if (widget.isDateComparison) _this.setRangeOptions(widget);else _this.setOptions(widget);
        _this.chartOptions.plotOptions.series.stacking = null;
        return _this;
    }

    _createClass(Bar, [{
        key: 'setOptions',
        value: function setOptions(bardata) {
            var _this2 = this;

            if (!(typeof bardata == "undefined")) {
                this.chartOptions.chart.type = 'column';
                if (!(typeof bardata.widgetData == "undefined") && bardata.widgetData != null) {
                    var series = [],
                        data_points = bardata.widgetData.dataPoints,
                        metricsList = bardata.metrics;
                    if (!(typeof data_points == 'undefined')) {
                        _.each(data_points, function (value) {
                            var searchObj = _.first(_.where(metricsList, { name: value['name'] })),
                                displayName = searchObj ? searchObj.displayName : value['name'];
                            var data = value['data'].map(function (item, i) {
                                return {
                                    color: data_points.length === 1 ? _this2.chartOptions.colors[i % 10] : null,
                                    y: parseFloat(item)
                                };
                            });
                            series.push({ 'name': displayName, data: data });
                        });
                    } else {
                        series.push({ 'name': '', 'data': '' });
                    }

                    this.chartOptions.legend.labelFormatter = function () {
                        return this.name;
                    };
                    this.setCommonOptions();
                    this.chartOptions.xAxis.categories = bardata.widgetData.categories;
                    this.chartOptions.series = series;

                    if (data_points && data_points.length === 1) {
                        this.chartOptions.legend.symbolHeight = 0;
                        this.chartOptions.legend.symbolWidth = 0;
                        this.chartOptions.legend.symbolRadius = 0;
                    }
                }
                // console.log(this.chartOptions)
            }
        }
    }, {
        key: 'setRangeOptions',
        value: function setRangeOptions(bardata) {
            var _this3 = this;

            if (typeof bardata != 'undefined') {
                this.chartOptions.chart.type = 'column';
                if (!(typeof bardata.widgetData == "undefined") && bardata.widgetData != null) {
                    var series = [],
                        data_points = bardata.widgetData.dataPoints,
                        metricsList = bardata.metrics;
                    if (typeof data_points != 'undefined') {
                        if (typeof data_points.dateRange1 != 'undefined') {
                            _.each(data_points.dateRange1, function (item, index) {
                                var searchObj = _.first(_.where(metricsList, { name: item['name'] })),
                                    displayName = searchObj ? searchObj.displayName : item['name'];
                                series.push({ 'cname': item.name + " " + DateRange, 'name': displayName, 'data': item.data.map(parseFloat), 'stack': "range1", 'color': _this3.comparisionColors[index] });
                                if (typeof data_points.dateRange2[index] != 'undefined') series.push({ 'cname': data_points.dateRange2[index].name + " " + CompDateRange, 'stack': "range2", 'name': displayName, 'data': data_points.dateRange2[index].data.map(parseFloat), 'color': _this3.getColor[index] });
                            });
                        }
                    } else {
                        series.push({ 'name': '', 'data': '' });
                    }
                    var CompDateRange = "",
                        DateRange = "";
                    var options = { weekday: 'narrow', year: 'numeric', month: 'short', day: 'numeric' };
                    if (bardata.isDateComparison) {
                        DateRange = helper.dateChange(new Date(parseInt(bardata.fromTime)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(bardata.toTime)).toLocaleString('en-US', options));
                        CompDateRange = helper.dateChange(new Date(parseInt(bardata.fromTime1)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(bardata.toTime1)).toLocaleString('en-US', options));
                    }

                    this.chartOptions.legend.labelFormatter = function () {
                        if (this.index % 2 == 0) return this.name + DateRange;else return this.name + CompDateRange;
                    };
                    this.setCommonOptions();
                    this.chartOptions.xAxis.categories = bardata.widgetData.categories;
                    this.chartOptions.series = series;
                }
            }
        }
    }, {
        key: 'setCommonOptions',
        value: function setCommonOptions() {
            this.chartOptions.legend.adjustChartSize = true;
            this.chartOptions.legend.itemStyle = {
                font: '9pt Open Sans, arial, sans-serif',
                color: 'black',
                fontWeight: 'normal'
            };
            this.chartOptions.tooltip = {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:,.2f} </b></td></tr>',
                footerFormat: '</table>', shared: true, useHTML: true
            }, this.chartOptions.xAxis.crosshair = true;
            this.chartOptions.yAxis.labels = {
                formatter: function formatter() {
                    return this.axis.defaultLabelFormatter.call(this);
                },
                style: {
                    fontSize: '9px'
                },
                x: 0,
                y: -2
            };
            this.chartOptions.yAxis.gridLineColor = '#EFEFEF';
            //for lables
            this.chartOptions["plotOptions"]["series"]["dataLabels"] = {
                enabled: true,
                formatter: this.labelFormatter
            };

            this.chartOptions.legend.symbolHeight = 8;
            this.chartOptions.legend.symbolWidth = 8;
            this.chartOptions.legend.symbolRadius = 6;

            this.chartOptions.xAxis.tickInterval = 0;
        }
    }, {
        key: 'labelFormatter',
        value: function labelFormatter() {
            var num = this.y;
            var isNegative = false;
            var formattedNumber;
            if (num < 0) {
                isNegative = true;
            }
            num = Math.abs(num);
            if (num >= 1000000000) {
                formattedNumber = (num / 1000000000).toFixed(2).replace(/\.0$/, '') + 'G';
            } else if (num >= 1000000) {
                formattedNumber = (num / 1000000).toFixed(2).replace(/\.0$/, '') + 'M';
            } else if (num >= 1000) {
                formattedNumber = (num / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
            } else {
                formattedNumber = num;
            }
            if (isNegative) {
                formattedNumber = '-' + formattedNumber;
            }
            return formattedNumber;
        }
    }, {
        key: 'getOptions',
        value: function getOptions() {
            return this.chartOptions;
        }
    }, {
        key: 'exportCSV',
        value: function exportCSV(widget, filename) {
            if (!widget.isDateComparison) {
                this.exportCSVNormal(widget, filename);
            } else {
                this.exportCSVCompareWith(widget, filename);
            }
        }
    }, {
        key: 'exportCSVNormal',
        value: function exportCSVNormal(widget, filename) {
            var metrics = widget.metrics,
                dimensions = widget.dimensions,
                widgetData = widget.widgetData;

            var dimensionFlags = {},
                metricFlags = {};

            var dimensionDisplayName = dimensions.map(function (item, i) {
                dimensionFlags[item.name] = i;
                return item.displayName;
            });
            var metricDisplayName = metrics.map(function (item, i) {
                metricFlags[item.name] = i + dimensions.length;
                return item.displayName;
            });
            var header = common.headerToCSV(dimensionDisplayName.concat(metricDisplayName));

            var csvdata = '';
            csvdata = csvdata + header + '\r\n';
            var array = [];

            for (var i = 0; i < widgetData.dataPoints.length; i++) {
                for (var j = 0; j < widgetData.categories.length; j++) {
                    var obj = array[j] ? array[j] : new Array(dimensions.length + metrics.length);
                    obj[0] = widgetData.categories[j];
                    var index = metricFlags[widgetData.dataPoints[i].name];
                    obj[index] = widgetData.dataPoints[i].data[j];
                    if (!array[j]) {
                        array.push(obj);
                    }
                }
            }
            csvdata = csvdata + common.json2csv(array);
            common.opencsv(csvdata, filename);
        }
    }, {
        key: 'exportCSVCompareWith',
        value: function exportCSVCompareWith(widget, filename) {
            var metrics = widget.metrics,
                dimensions = widget.dimensions,
                widgetData = widget.widgetData;

            var dimensionFlags = {},
                metricFlags = {};

            var dimensionDisplayName = dimensions.map(function (item, i) {
                dimensionFlags[item.name] = i + 2;
                return item.displayName;
            });
            var metricDisplayName = metrics.map(function (item, i) {
                metricFlags[item.name] = i + dimensions.length + 2;
                return item.displayName;
            });
            dimensionDisplayName.splice(0, 0, "Start Date");
            dimensionDisplayName.splice(1, 0, "End Date");

            var header = common.headerToCSV(dimensionDisplayName.concat(metricDisplayName));
            var csvdata = '';
            csvdata = csvdata + header + '\r\n';
            var array1 = [];
            var array2 = [];

            var fromTime = moment(parseInt(widget.fromTime)).format('MMM DD YYYY');
            var fromTime1 = moment(parseInt(widget.fromTime1)).format('MMM DD YYYY');
            var toTime = moment(parseInt(widget.toTime)).format('MMM DD YYYY');
            var toTime1 = moment(parseInt(widget.toTime1)).format('MMM DD YYYY');

            for (var i = 0; i < Object.keys(widgetData.dataPoints).length; i++) {
                var dateRange = Object.keys(widgetData.dataPoints)[i];
                if (dateRange != 'percentChange') {
                    (function () {
                        var startDate = dateRange == "dateRange1" ? fromTime : fromTime1;
                        var endDate = dateRange == "dateRange1" ? toTime : toTime1;
                        var array = dateRange == "dateRange1" ? array1 : array2;
                        var categories = _.uniq(widgetData.categories);
                        _.each(widgetData.dataPoints[dateRange], function (value) {
                            for (var k = 0; k < categories.length; k++) {
                                var obj = array[k] ? array[k] : new Array(dimensions.length + metrics.length + 2);
                                obj[0] = startDate;
                                obj[1] = endDate;
                                obj[2] = categories[k];
                                var index = metricFlags[value.name];
                                obj[index] = value.data[k];
                                if (!array[k]) {
                                    array.push(obj);
                                }
                            }
                        });
                    })();
                }
            }
            csvdata = csvdata + common.json2csv(array1.concat(array2));
            common.opencsv(csvdata, filename);
        }
    }]);

    return Bar;
}(Chart);

module.exports = Bar;