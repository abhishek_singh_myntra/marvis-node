'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by 12072 on 8/9/16.
 */
var Chart = require('./Chart');
var _ = require('underscore');
var helper = require('../../../../utils/helper');
var SmartChartWrapper = require('./SmartChartWrapper');
var ErrorMessage = require('../ErrorMessage');
var common = require('../../../../utils/common');

var Bubble = function (_Chart) {
    _inherits(Bubble, _Chart);

    function Bubble(props) {
        _classCallCheck(this, Bubble);

        var _this = _possibleConstructorReturn(this, (Bubble.__proto__ || Object.getPrototypeOf(Bubble)).call(this, props));

        var widget = props.widget;
        if (widget.isDateComparison) _this.setRangeOptions(widget);else _this.setOptions(widget);

        _this.overrideChartOptionsByWidgetType();
        _this.exportCSV = _this.exportCSV.bind(_this);

        return _this;
    }

    _createClass(Bubble, [{
        key: 'overrideChartOptionsByWidgetType',
        value: function overrideChartOptionsByWidgetType() {}
    }, {
        key: 'setOptions',
        value: function setOptions(bubblechartdata) {
            var series = [];
            if (!(typeof bubblechartdata == "undefined")) {
                var metrics = _.pluck(bubblechartdata.metrics, 'name');
                var metrics_displayName = _.pluck(bubblechartdata.metrics, 'displayName');
                var dimension_name = _.pluck(bubblechartdata.dimensions, 'name')[0];
                var legend = _.pluck(bubblechartdata.metrics, 'displayName').join(', ') + ' by ' + _.pluck(bubblechartdata.dimensions, 'displayName').join(', ');
                this.chartOptions.chart.type = 'bubble';
                this.chartOptions.tooltip = {
                    pointFormat: '<span style="font-size:10px">  <b>{point.dimension}</b>' + '<br />' + metrics_displayName[0] + ': <b>{point.x}</b>' + '<br />' + metrics_displayName[1] + ' : <b>{point.y}</b>' + '<br />' + metrics_displayName[2] + ': <b>{point.z}</b></span><br />',
                    shared: true,
                    useHTML: true,
                    headerFormat: '<span />'
                };
                this.chartOptions.xAxis = {
                    gridLineWidth: 1,
                    title: {
                        text: metrics_displayName[0]
                    },
                    plotLines: [{
                        color: 'black',
                        dashStyle: 'dot',
                        width: 2,
                        label: {
                            rotation: 0,
                            y: 15,
                            style: {
                                fontStyle: 'italic'
                            }
                        },
                        zIndex: 3
                    }]
                };
                this.chartOptions.yAxis = {
                    startOnTick: false,
                    endOnTick: false,
                    title: {
                        text: metrics_displayName[1]
                    },
                    maxPadding: 0.2,
                    plotLines: [{
                        color: 'black',
                        dashStyle: 'dot',
                        width: 2,
                        label: {
                            align: 'right',
                            style: {
                                fontStyle: 'italic'
                            },
                            x: -10
                        },
                        zIndex: 3
                    }]
                };
                this.chartOptions.plotOptions.series = {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                };
                this.chartOptions.showInLegend = true;
                this.chartOptions.series.type = 'bubble';
                this.chartOptions.xAxis.crosshair = false;

                if (!(typeof bubblechartdata.widgetData.result == 'undefined')) {
                    _.each(bubblechartdata.widgetData.result, function (value) {
                        series.push({ 'dimension': value[dimension_name], 'x': parseFloat(value[metrics[0]].replace(/,/g, '')), 'y': parseFloat(value[metrics[1]].replace(/,/g, '')), 'z': parseFloat(value[metrics[2]].replace(/,/g, '')) });
                    });
                } else {
                    series.push({ 'dimension': '', 'x': '', 'y': '', 'z': '' });
                }
                this.chartOptions.legend.adjustChartSize = true;
                this.chartOptions.legend.itemStyle = {
                    font: '9pt Open Sans, arial, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                };
                this.chartOptions.legend.symbolHeight = 8;
                this.chartOptions.legend.symbolWidth = 8;
                this.chartOptions.legend.symbolRadius = 6;

                this.chartOptions.series = [{
                    name: legend,
                    data: series
                }];
            }
        }
    }, {
        key: 'setRangeOptions',
        value: function setRangeOptions(widget) {
            var options = { weekday: 'narrow', year: 'numeric', month: 'short', day: 'numeric' };
            var DateRange = helper.dateChange(new Date(parseInt(widget.fromTime)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(widget.toTime)).toLocaleString('en-US', options));
            var CompDateRange = helper.dateChange(new Date(parseInt(widget.fromTime1)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(widget.toTime1)).toLocaleString('en-US', options));
            var metrics_displayName = _.pluck(widget.metrics, 'displayName');
            this.chartOptions.chart.type = 'bubble';
            var legend = _.pluck(widget.metrics, 'displayName').join(', ') + ' by ' + _.pluck(widget.dimensions, 'displayName').join(', ');

            this.chartOptions.tooltip = {
                pointFormat: '<span style="font-size:10px"><b>  {point.dimension}</b>' + '<br />' + metrics_displayName[0] + ': <b>{point.x}</b>' + '<br />' + metrics_displayName[1] + ' : <b>{point.y}</b>' + '<br />' + metrics_displayName[2] + ': <b>{point.z}</b></span><br />',
                shared: true,
                useHTML: true,
                headerFormat: '<span />'
            };
            this.chartOptions.xAxis = {
                gridLineWidth: 1,
                title: {
                    text: metrics_displayName[0]
                },
                plotLines: [{
                    color: 'black',
                    dashStyle: 'dot',
                    width: 2,
                    label: {
                        rotation: 0,
                        y: 15,
                        style: {
                            fontStyle: 'italic'
                        }
                    },
                    zIndex: 3
                }]
            };
            this.chartOptions.yAxis = {
                startOnTick: false,
                endOnTick: false,
                title: {
                    text: metrics_displayName[1]
                },
                labels: {
                    format: '{value}'
                },
                maxPadding: 0.2,
                plotLines: [{
                    color: 'black',
                    dashStyle: 'dot',
                    width: 2,
                    label: {
                        align: 'right',
                        style: {
                            fontStyle: 'italic'
                        },
                        x: -10
                    },
                    zIndex: 3
                }]
            };
            this.chartOptions.plotOptions.series = {
                dataLabels: {
                    enabled: false,
                    format: '{point.name}'
                }
            };

            this.chartOptions.showInLegend = true;
            this.chartOptions.series.type = 'bubble';
            this.chartOptions.xAxis.crosshair = false;

            var widgetData = widget.widgetData;

            var metrics = _.pluck(widget.metrics, 'name');

            var series = [];
            var data1 = [],
                data2 = [];
            widgetData.result.map(function (item) {
                var dimensionsRow;
                Object.keys(item.dimensions).map(function (key) {
                    dimensionsRow = item.dimensions[key];
                });
                var dateRow1 = item.dateRange1;
                var dateRow2 = item.dateRange2;
                data1.push({ 'x': parseFloat(dateRow1[metrics[0]].replace(/,/g, '')), 'y': parseFloat(dateRow1[metrics[1]].replace(/,/g, '')), 'z': parseFloat(dateRow1[metrics[2]].replace(/,/g, '')), 'dimension': dimensionsRow });
                data2.push({ 'x': parseFloat(dateRow2[metrics[0]].replace(/,/g, '')), 'y': parseFloat(dateRow2[metrics[1]].replace(/,/g, '')), 'z': parseFloat(dateRow2[metrics[2]].replace(/,/g, '')), 'dimension': dimensionsRow });
            });
            this.chartOptions.legend.adjustChartSize = true;
            this.chartOptions.legend.itemStyle = {
                font: '9pt Open Sans, arial, sans-serif',
                color: 'black',
                fontWeight: 'normal'
            };
            this.chartOptions.legend.symbolHeight = 8;
            this.chartOptions.legend.symbolWidth = 8;
            this.chartOptions.legend.symbolRadius = 6;

            this.chartOptions.series = [{
                name: legend + "- " + DateRange,
                data: data1
            }, {
                name: legend + "- " + CompDateRange,
                data: data2
            }];
        }
    }, {
        key: 'getOptions',
        value: function getOptions() {
            return this.chartOptions;
        }
    }, {
        key: 'exportCSV',
        value: function exportCSV(widget, filename) {
            if (!widget.isDateComparison) {
                this.exportCSVNormal(widget, filename);
            } else {
                this.exportCSVCompareWith(widget, filename);
            }
        }
    }, {
        key: 'exportCSVNormal',
        value: function exportCSVNormal(widget, filename) {
            var metrics = widget.metrics,
                dimensions = widget.dimensions,
                widgetData = widget.widgetData;

            var metricsData = metrics.map(function (item) {
                return _extends({}, item);
            });
            var columnMetadata = dimensions.concat(metricsData).map(function (item) {
                return {
                    columnName: item.name,
                    displayName: item.displayName
                };
            });
            var header = {},
                i = 0,
                j = 0;
            columnMetadata.map(function (item, index) {
                if (item.columns) {
                    header[i++] = index === 0 ? "" : item.displayName;
                } else {
                    header[i++] = item.displayName;
                }
            });
            var csv = common.json2csv([header]);
            csv += common.json2csv(widgetData.result);
            common.opencsv(csv, filename);
        }
    }, {
        key: 'exportCSVCompareWith',
        value: function exportCSVCompareWith(widget, filename) {
            var metrics = widget.metrics,
                dimensions = widget.dimensions,
                widgetData = widget.widgetData;

            var dimensionFlags = {},
                metricFlags = {};

            var dimensionDisplayName = dimensions.map(function (item, i) {
                dimensionFlags[item.name] = i + 2;
                return item.displayName;
            });
            var metricDisplayName = metrics.map(function (item, i) {
                metricFlags[item.name] = i + dimensions.length + 2;
                return item.displayName;
            });
            dimensionDisplayName.splice(0, 0, "Start Date");
            dimensionDisplayName.splice(1, 0, "End Date");

            var header = common.headerToCSV(dimensionDisplayName.concat(metricDisplayName));
            var csvdata = '';
            csvdata = csvdata + header + '\r\n';
            var array = [];
            var metricsName = _.pluck(metrics, 'name');

            var fromTime = moment(parseInt(widget.fromTime)).format('MMM DD YYYY');
            var fromTime1 = moment(parseInt(widget.fromTime1)).format('MMM DD YYYY');
            var toTime = moment(parseInt(widget.toTime)).format('MMM DD YYYY');
            var toTime1 = moment(parseInt(widget.toTime1)).format('MMM DD YYYY');

            widgetData.result.map(function (item) {
                var dateRow1 = item.dateRange1;
                var dateRow2 = item.dateRange2;
                var obj1 = new Array(metrics.length + dimensions.length + 2),
                    obj2 = new Array(metrics.length + dimensions.length + 2);
                obj1[0] = fromTime;obj1[1] = toTime;
                obj2[0] = fromTime1;obj2[1] = toTime1;

                _.each(Object.keys(item.dimensions), function (value) {
                    var index = dimensionFlags[value];
                    obj1[index] = item.dimensions[value];
                    obj2[index] = item.dimensions[value];
                });

                _.each(metricsName, function (value) {
                    var index = metricFlags[value];
                    obj1[index] = dateRow1[value];
                });
                _.each(metricsName, function (value) {
                    var index = metricFlags[value];
                    obj2[index] = dateRow2[value];
                });
                array.push(obj1);
                array.push(obj2);
            });
            csvdata = csvdata + common.json2csv(array);
            common.opencsv(csvdata, filename);
        }
    }]);

    return Bubble;
}(Chart);

module.exports = Bubble;