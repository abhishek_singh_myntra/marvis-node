'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by 12072 on 8/17/16.
 */
var getDefaultColors = function getDefaultColors() {
    return {
        '0': '#33ADFA',
        '1': '#ffd933',
        '2': '#b7ff33',
        '3': '#ff57c7',
        '4': '#33e5ff',
        '5': '#ffb034',
        '6': '#be53ee',
        '7': '#3357ed',
        '8': '#ffff33',
        '9': '#ff8666',
        '10': '#35EB34'
    };
};

var getDefaultComparisionColors = function getDefaultComparisionColors() {
    return {
        '0': '#35EB34',
        '1': '#ff8666',
        '2': '#ffff33',
        '3': '#3357ed',
        '4': '#be53ee',
        '5': '#ffb034',
        '6': '#33e5ff',
        '7': '#ff57c7',
        '8': '#b7ff33',
        '9': '#ffd933',
        '10': '#33ADFA'
    };
};

var Chart = function () {
    function Chart(props) {
        _classCallCheck(this, Chart);

        // this.setGlobalOptions();
        this.chartOptions = {
            credits: {
                enabled: false
            },
            colors: ['#3093d1', '#fcb631', '#9ad631', '#f34ea8', '#30bfdd', '#fc9532', '#9f4bc7', '#304ec6', '#fcd731', '#f08576', '#418842'],
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                //zoomType: 'x',
                subtitle: {
                    text: null
                },
                tooltip: { "shared": true, "useHTML": false },
                type: 'column'
            },
            title: {
                text: null

            },
            xAxis: {
                //tickInterval: 0,
                type: 'linear',
                title: {
                    text: null
                },
                labels: {
                    style: {
                        fontSize: '9px'
                    }
                },
                categories: [],
                crosshair: false
            },
            yAxis: {
                labels: {
                    style: {
                        fontSize: '9px'
                    },
                    gridLineColor: '#EFEFEF',
                    align: 'left',
                    x: 0,
                    y: -2
                },
                title: {
                    text: ''
                }
            },

            plotOptions: {
                column: {
                    pointPadding: 0,
                    borderWidth: 0
                },
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    }
                },
                series: {
                    stacking: null
                }
            },
            legend: {
                itemStyle: {
                    font: '9pt Trebuchet MS, Verdana, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                },
                itemHoverStyle: {
                    color: 'gray'
                },
                title: "",
                verticalAlign: 'top',
                borderWidth: 0,
                enabled: true
            },
            series: [],

            navigation: {
                buttonOptions: {
                    verticalAlign: 'top',
                    y: 0,
                    enabled: false
                }
            },
            exporting: {
                enabled: false
            }
        };
        var colors = props.colors || {};
        var comparisionColors = props.colors || {};
        var defaultColors = getDefaultColors();
        var defaultComparisionColors = getDefaultComparisionColors();
        this.getColor = Object.assign({}, defaultColors, colors);
        this.comparisionColors = Object.assign({}, defaultComparisionColors, comparisionColors);
        this.chart;
        this.setChart = this.setChart.bind(this);
        this.exportChart = this.exportChart.bind(this);
    }

    _createClass(Chart, [{
        key: 'setChart',
        value: function setChart(chart) {
            this.chart = chart;
        }
    }, {
        key: 'getChart',
        value: function getChart() {
            var chart = this.chart && this.chart.getChart();
            return chart;
        }
    }, {
        key: 'exportChart',
        value: function exportChart(type, filename) {
            var chart = this.chart && this.chart.getChart();
            if (chart) {
                chart.exportChart({ type: type, filename: filename });
            }
        }
    }, {
        key: 'setGlobalOptions',
        value: function setGlobalOptions() {
            Highcharts.setOptions({
                global: {
                    //VMLRadialGradientURL: 'http://code.highcharts.com/{version}/gfx/vml-radial-gradient.png',  // Path to the pattern image required by VML browsers in order to draw radial gradients.
                    //canvasToolsURL: 'http://code.highcharts.com/{versihon}/modules/canvas-tools.js',            // The URL to the additional file to lazy load for Android 2.x devices. These devices don't support SVG, so we download a helper file that contains canvg, its dependecy rbcolor, and our own CanVG Renderer class.
                    timezoneOffset: 0, // The timezone offset in minutes
                    useUTC: true // Whether to use UTC time for axis scaling, tickmark placement and time display in Highcharts.dateFormat.
                },
                lang: {
                    //contextButtonTitle: 'Chart context menu',  // Exporting module menu. The tooltip title for the context menu holding print and export menu items.
                    decimalPoint: '.', // The default decimal point used in the Highcharts.numberFormat method unless otherwise specified in the function arguments.
                    //downloadJPEG: 'Download JPEG image',       // Exporting module only. The text for the JPEG download menu item.
                    //downloadPDF: 'Download PDF document',      // Exporting module only. The text for the PDF download menu item.
                    //downloadSVG: 'Download SVG vector image',  // Exporting module only. The text for the SVG download menu item.
                    drillUpText: 'Back to {series.name}', // The text for the button that appears when drilling down, linking back to the parent series.
                    loading: 'Loading...', // The loading text that appears when the chart is set into the loading state following a call to chart.showLoading.
                    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'], // An array containing the months names. Corresponds to the %B format in Highcharts.dateFormat().
                    noData: 'No data to display', // The text to display when the chart contains no data. Requires the no-data module, see noData.
                    numericSymbols: ['k', 'M', 'G', 'T', 'P', 'E'], // Metric prefixes used to shorten high numbers in axis labels. Replacing any of the positions with null causes the full number to be written.
                    //printChart: 'Print chart',  // Exporting module only. The text for the menu item to print the chart.
                    resetZoom: 'Reset zoom', // The text for the label appearing when a chart is zoomed.
                    resetZoomTitle: 'Reset zoom level 1:1', // The tooltip title for the label appearing when a chart is zoomed.
                    shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], // An array containing the months names in abbreviated form. Corresponds to the %b format in Highcharts.dateFormat().
                    thousandsSep: ',', // The default thousands separator used in the Highcharts.numberFormat method unless otherwise specified in the function arguments.
                    weekdays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] // An array containing the weekday names.
                },
                theme: {
                    //colors: ['#058dc7', '#50B432', '#5c5c5c', '#f9a831', '#bf2baa', '#fa1a9e', '#f2d220', '#ee3239']
                    colors: ['#007ac7', '#ffa600', '#84ce00', '#f42494', '#00b2d6', '#ff7d01', '#8b20bb', '#0024ba', '#ffcf00', '#ff2000', '#02b801']
                },
                chart: {
                    style: {
                        fontFamily: '"Open Sans",arial,sans-serif'
                    }
                },
                labels: {
                    style: {
                        fontSize: '9px'
                    }
                }

            });
        }
    }]);

    return Chart;
}();

module.exports = Chart;