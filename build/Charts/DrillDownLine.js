'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by 12072 on 9/18/16.
 */
var Chart = require('./Chart');
var _ = require('underscore');
var helper = require('../../../../utils/helper');
var SmartRadioButton = require('../../common/SmartRadioButton');
var SmartChartWrapper = require('./SmartChartWrapper');
var ErrorMessage = require('../ErrorMessage');
var common = require('../../../../utils/common');

var Line = function (_Chart) {
    _inherits(Line, _Chart);

    function Line() {
        var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, Line);

        var _this = _possibleConstructorReturn(this, (Line.__proto__ || Object.getPrototypeOf(Line)).call(this, props));

        _this.chartOptions["plotOptions"]["series"] = { "stacking": null };
        _this.chartOptions["plotOptions"]["series"]["marker"] = { "symbol": 'circle' };
        _this.chartOptions["plotOptions"]["pie"]["dataLabels"] = { enabled: false };

        _this.setProps = _this.setProps.bind(_this);
        _this.onLineClick = _this.onLineClick.bind(_this);
        return _this;
    }

    _createClass(Line, [{
        key: 'setProps',
        value: function setProps(props) {
            this.widget = props.widget;

            if (this.getChart()) {
                return this.updateChart(props);
            }
            if (this.widget.isDateComparison) this.setRangeOptions();else this.setOptions();

            this.overrideChartOptionsByWidgetType();
        }
    }, {
        key: 'overrideChartOptionsByWidgetType',
        value: function overrideChartOptionsByWidgetType() {}
    }, {
        key: 'updateChart',
        value: function updateChart(props) {
            var chart = this.getChart();
            var dataPoints = props.widget.widgetData.dataPoints;
            var metricsList = props.widget.metrics;

            this.chartOptions.xAxis.categories = props.widget.widgetData.categories;
            this.chartOptions.series[0].data = props.widget.widgetData.dataPoints;
            var metricYaxisMap = this.getMetricAxisMap(metricsList);
            if (this.drilldown && this.drilldown.name) {
                // var currentDD = _.where(dataPoints, {name: this.drilldown.name});
                var drilldown = this.constructDrillDownPoints(dataPoints, metricYaxisMap, this.drilldown.name);
                this.setDrillDown(chart, drilldown.data, "Revenue By Minutes");
            } else {
                this.setDrillDown(chart, this.constructDataPoints(dataPoints, metricYaxisMap), "Revenue By Hour");
            }
        }
    }, {
        key: 'setDrillDown',
        value: function setDrillDown(chart, data, title) {
            var _this2 = this;

            while (chart.series.length > 0) {
                chart.series[0].remove(true);
            }chart.setTitle({ text: title, x: -20 });
            _.each(data, function (dataSet, index) {
                chart.addSeries({
                    name: dataSet.name,
                    data: dataSet.data,
                    yAxis: dataSet.yAxis,
                    zoneAxis: 'x',
                    zones: [{
                        value: dataSet.data.length - 2
                    }, {
                        dashStyle: 'dot'
                    }]
                }, false);
                chart.series[index].options.color = _this2.chartOptions.colors[index];
                chart.series[index].options.marker.symbol = 'circle';
                chart.series[index].update(chart.series[index].options);
            });
            chart.redraw();
        }
    }, {
        key: 'onLineClick',
        value: function onLineClick(drillDown) {
            var drilldown = drillDown;
            this.drilldown = drilldown;
            var chart = this.getChart();
            if (drilldown) {
                this.setDrillDown(chart, drilldown.data, "Revenue By Minutes");
            } else {
                this.setOptions();
                this.setDrillDown(chart, this.chartOptions.series, "Revenue By Hour");
            }
        }
    }, {
        key: 'constructDrillDownPoints',
        value: function constructDrillDownPoints(data_points, metricAxisMap, itemKey) {

            return {
                name: itemKey,
                data: _.chain(data_points).map(function (drillDown) {
                    var dataItem = _.findWhere(drillDown.data, { key: itemKey });
                    if (dataItem) {
                        return {
                            data: _.map(dataItem.data, function (i) {
                                return [i.key, parseFloat(i.value)];
                            }),
                            name: drillDown.key + "-" + itemKey + "-Hour",
                            yAxis: metricAxisMap[drillDown.key]
                        };
                    }
                    return null;
                }).compact().value()
            };
        }
    }, {
        key: 'constructDataPoints',
        value: function constructDataPoints(data_points, metricAxisMap) {
            var _this3 = this;

            return _.map(data_points, function (dataSet, inx) {
                return {
                    name: dataSet.key,
                    data: _.map(dataSet.data, function (item) {
                        return {
                            y: parseFloat(item.value),
                            name: item.key,
                            drilldown: _this3.constructDrillDownPoints(data_points, metricAxisMap, item.key)
                        };
                    }),
                    color: _this3.chartOptions.colors[inx],
                    yAxis: metricAxisMap[dataSet.key],
                    zoneAxis: 'x',
                    zones: [{
                        value: dataSet.data.length - 2
                    }, {
                        dashStyle: 'dot'
                    }]
                };
            });
        }
    }, {
        key: 'getMetricAxisMap',
        value: function getMetricAxisMap(metricsList) {
            var metricAxisMap = {};
            for (var i = 0; i < metricsList.length; i++) {
                metricAxisMap[metricsList[i].name] = i;
            }
            return metricAxisMap;
        }
    }, {
        key: 'setOptions',
        value: function setOptions() {
            var self = this;
            var linedata = this.widget;
            if (!(typeof linedata == "undefined")) {
                var series = [],
                    metricsList = linedata.metrics,
                    data_points = linedata.widgetData.dataPoints;

                if (!(typeof data_points == 'undefined')) {
                    series = this.constructDataPoints(data_points, this.getMetricAxisMap(metricsList));
                } else {
                    series.push({ 'name': '', 'data': '' });
                }
                this.chartOptions.yAxis = _.map(this.widget.widgetData.dataPoints, function (dpoints, inx) {
                    return {
                        labels: {
                            formatter: function formatter() {
                                return this.axis.defaultLabelFormatter.call(this);
                                //return this.value;
                            },
                            style: {
                                fontSize: '9px'
                            }
                        },
                        title: {
                            text: dpoints.key || "",
                            style: { color: '#000000' }
                        },
                        opposite: inx === 0 ? false : true
                    };
                });

                // this.chartOptions.xAxis.categories = linedata.widgetData.categories;
                this.chartOptions.xAxis.crosshair = true;
                this.chartOptions.chart.type = 'line';

                this.chartOptions.plotOptions.line = {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function click() {
                                self.onLineClick(this.drilldown);
                            }
                        }
                    }
                };

                this.chartOptions.exporting.buttons = {
                    contextButton: {
                        enabled: true
                    }
                };

                this.chartOptions.legend.labelFormatter = function () {
                    return this.name;
                };
                this.chartOptions.legend.adjustChartSize = true;
                this.chartOptions.legend.itemStyle = {
                    font: '9pt Open Sans, arial, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                };
                this.chartOptions.legend.symbolHeight = 8;
                this.chartOptions.legend.symbolWidth = 8;
                this.chartOptions.legend.symbolRadius = 6;

                this.chartOptions.tooltip = {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:,.2f}</b></td></tr>',
                    footerFormat: '</table>', shared: true, useHTML: true
                };
                this.chartOptions.series = series;
            }
        }
    }, {
        key: 'setRangeOptions',
        value: function setRangeOptions(linedata) {
            var _this4 = this;

            if (!(typeof linedata == "undefined")) {
                var series = [],
                    metricsList = linedata.metrics,
                    data_points = linedata.widgetData.dataPoints,
                    stepNo = 1,
                    count = 0,
                    opp = true,
                    displayNamesList = [],
                    metricAxisMap = {};

                for (var i = 0; i < metricsList.length; i++) {
                    metricAxisMap[metricsList[i].name] = i;
                }

                if (typeof data_points != 'undefined') {
                    var CompDateRange = "",
                        DateRange = "";
                    var options = { weekday: 'narrow', year: 'numeric', month: 'short', day: 'numeric' };
                    if (linedata.isDateComparison) {
                        DateRange = helper.dateChange(new Date(parseInt(linedata.fromTime)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(linedata.toTime)).toLocaleString('en-US', options));
                        CompDateRange = helper.dateChange(new Date(parseInt(linedata.fromTime1)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(linedata.toTime1)).toLocaleString('en-US', options));
                    }
                    if (typeof data_points.dateRange1 != 'undefined') {
                        _.each(data_points.dateRange1, function (item, index) {
                            var searchObj = _.first(_.where(metricsList, { name: item['name'] })),
                                displayName = searchObj ? searchObj.displayName : item['name'],
                                subName = item["dimensionValue"] ? " - " + item["dimensionValue"] : "",
                                finalDisplayName = displayName + subName;

                            displayNamesList.push(finalDisplayName);
                            series.push({ 'name': finalDisplayName,
                                'data': item.data,
                                'color': _this4.comparisionColors[index],
                                'yAxis': metricAxisMap[searchObj.name]
                            });
                            if (typeof data_points.dateRange2[index] != 'undefined') {
                                series.push({ 'name': finalDisplayName,
                                    'data': data_points.dateRange2[index].data,
                                    'color': _this4.getColor[index],
                                    'yAxis': metricAxisMap[searchObj.name]
                                });
                            }
                        });
                        count = data_points.dateRange1[0].data.length;
                    }
                } else {
                    series.push({ 'name': '', 'data': '' });
                }
                if (linedata.isDateComparison) {
                    DateRange = helper.dateChange(new Date(parseInt(linedata.fromTime)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(linedata.toTime)).toLocaleString('en-US', options));
                    CompDateRange = helper.dateChange(new Date(parseInt(linedata.fromTime1)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(linedata.toTime1)).toLocaleString('en-US', options));
                }

                this.chartOptions.legend.labelFormatter = function () {
                    if (this.index % 2 == 0) return this.name + " - " + DateRange;else return this.name + " - " + CompDateRange;
                };
                var yaxisobjs = [],
                    opp = true,
                    metricsarray = [],
                    onlyOneYAxis = false;
                metricsarray = linedata.widgetData.metrics.split(',');

                if (linedata.widgetData.dimensions) {
                    var dimArray = linedata.widgetData.dimensions.split(',');
                    if (dimArray.length > 1 && metricsarray.length == 1) {
                        onlyOneYAxis = true;
                    }
                }

                for (var i = 0; i < displayNamesList.length; i++) {
                    if (onlyOneYAxis) {
                        opp = false;
                    } else {
                        opp = !opp;
                    }
                    var a = {
                        labels: {
                            formatter: function formatter() {
                                return this.axis.defaultLabelFormatter.call(this);
                            },
                            align: opp ? 'left' : 'right',
                            x: 0,
                            y: -2
                        },
                        style: {
                            fontSize: '9px'
                        },
                        title: {
                            text: " "
                        },
                        opposite: !opp,
                        showEmpty: true,
                        offset: 0
                    };
                    if (i != 0) {
                        a["gridLineWidth"] = 0;
                    }
                    yaxisobjs.push(a);
                }
                this.chartOptions.yAxis = yaxisobjs;
                this.chartOptions.xAxis.categories = linedata.widgetData.categories.dateRange1;
                this.chartOptions.xAxis.crosshair = true;
                var legend_text = _.pluck(linedata.dimensions, 'name').join(', ') + '<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>';
                this.chartOptions.chart.type = 'line';
                this.chartOptions.xAxis.type = 'datetime';
                if (count > 10) {
                    count = count + 3;
                    stepNo = Math.ceil(count / 10);
                }
                this.chartOptions.xAxis.tickInterval = stepNo;
                this.chartOptions.legend.adjustChartSize = true;

                this.chartOptions.legend.itemStyle = {
                    font: '9pt Trebuchet MS, Verdana, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                };
                this.chartOptions.tooltip = {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:,.0f}</b></td></tr>',
                    footerFormat: '</table>', shared: true, useHTML: true
                };
                this.chartOptions.series = series;
            }
        }
    }, {
        key: 'getOptions',
        value: function getOptions() {
            return this.chartOptions;
        }
    }]);

    return Line;
}(Chart);

module.exports = DrillDownLine;