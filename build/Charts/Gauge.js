'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by 12072 on 8/9/16.
 */

var Chart = require('./Chart');
var _ = require('underscore');

var Gauge = function (_Chart) {
    _inherits(Gauge, _Chart);

    function Gauge(props) {
        _classCallCheck(this, Gauge);

        var _this = _possibleConstructorReturn(this, (Gauge.__proto__ || Object.getPrototypeOf(Gauge)).call(this, props));

        var widget = props.widget;
        _this.setCommonOptions();
        _this.setOptions(widget);

        _this.overrideChartOptionsByWidgetType(widget, props.barOptions);
        return _this;
    }

    _createClass(Gauge, [{
        key: 'overrideChartOptionsByWidgetType',
        value: function overrideChartOptionsByWidgetType(bardata) {
            var barOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            this.chartOptions = _.extend(this.chartOptions, barOptions);
        }
    }, {
        key: 'setOptions',
        value: function setOptions(bardata) {
            if (!(typeof bardata == "undefined") || true) {
                this.chartOptions.chart.type = 'solidgauge';
                this.chartOptions.yAxis = {
                    min: 0,
                    max: 200,
                    title: {
                        text: 'Speed'
                    }
                };
                this.chartOptions.series = [{
                    name: 'Speed',
                    data: [Math.round(Math.random() * 100)],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:25px;color:' + 'black' + '">{y}</span><br/>' + '<span style="font-size:12px;color:silver">km/h</span></div>'
                    },
                    tooltip: {
                        valueSuffix: ' km/h'
                    }
                }];
            }
        }
    }, {
        key: 'setCommonOptions',
        value: function setCommonOptions() {
            this.chartOptions.pane = {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            };
            this.chartOptions.yAxis = {
                stops: [[0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickPixelInterval: 400,
                tickWidth: 0,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            };
            this.chartOptions.plotOptions = {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            };
            this.chartOptions.credits = {
                enabled: false
            };
        }
    }, {
        key: 'labelFormatter',
        value: function labelFormatter() {
            var num = this.y;
            var isNegative = false;
            var formattedNumber;
            if (num < 0) {
                isNegative = true;
            }
            num = Math.abs(num);
            if (num >= 1000000000) {
                formattedNumber = (num / 1000000000).toFixed(2).replace(/\.0$/, '') + 'G';
            } else if (num >= 1000000) {
                formattedNumber = (num / 1000000).toFixed(2).replace(/\.0$/, '') + 'M';
            } else if (num >= 1000) {
                formattedNumber = (num / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
            } else {
                formattedNumber = num;
            }
            if (isNegative) {
                formattedNumber = '-' + formattedNumber;
            }
            return formattedNumber;
        }
    }, {
        key: 'getOptions',
        value: function getOptions() {
            return this.chartOptions;
        }
    }]);

    return Gauge;
}(Chart);

module.exports = Gauge;