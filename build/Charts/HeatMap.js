'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by 165322 on 11/11/16.
 */
var Chart = require('./Chart');
var _ = require('underscore');

var HeatMap = function (_Chart) {
    _inherits(HeatMap, _Chart);

    function HeatMap(props) {
        _classCallCheck(this, HeatMap);

        var _this = _possibleConstructorReturn(this, (HeatMap.__proto__ || Object.getPrototypeOf(HeatMap)).call(this, props));

        var widget = props.widget;
        if (widget.isDateComparison) _this.setRangeOptions(widget);else _this.setOptions(widget);

        _this.overrideChartOptionsByWidgetType();
        return _this;
    }

    _createClass(HeatMap, [{
        key: 'overrideChartOptionsByWidgetType',
        value: function overrideChartOptionsByWidgetType() {}
    }, {
        key: 'setOptions',
        value: function setOptions(heatmapdata) {
            var series = [];
            if (!(typeof heatmapdata == "undefined")) {
                var metrics = _.pluck(heatmapdata.metrics, 'name');
                var metrics_displayName = _.pluck(heatmapdata.metrics, 'displayName');
                var dimension_name = _.pluck(heatmapdata.dimensions, 'name')[0];
                var legend = _.pluck(heatmapdata.metrics, 'displayName').join(', ') + ' by ' + _.pluck(heatmapdata.dimensions, 'displayName').join(', ');
                this.chartOptions.chart.type = 'treemap';
                this.chartOptions.tooltip = {
                    pointFormat: '<span style="font-size:10px">  <b>{point.name}</b>' + '<br />' + metrics_displayName[0] + ': <b>{point.value}</b>' + '<br />' + metrics_displayName[1] + ': <b>{point.colorValue}</b>',
                    shared: true,
                    useHTML: true,
                    headerFormat: '<span />'
                };
                this.chartOptions.title = {
                    text: legend,
                    align: 'center',
                    verticalAlign: 'top',
                    y: 0,
                    style: {
                        fontSize: "1rem",
                        color: "#a09797"
                    }
                };
                this.chartOptions.plotOptions.series = {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                };
                this.chartOptions.showInLegend = true;
                this.chartOptions.xAxis.crosshair = false;

                if (!(typeof heatmapdata.widgetData.result == 'undefined')) {
                    _.each(heatmapdata.widgetData.result, function (value) {
                        if (parseFloat(value[metrics[1]].replace(/,/g, '')) <= 0) {
                            value[metrics[1]] = 0.0001.toString();
                        }
                        series.push({ 'name': value[dimension_name], 'value': parseFloat(value[metrics[0]].replace(/,/g, '')), 'colorValue': parseFloat(value[metrics[1]].replace(/,/g, '')) });
                    });
                } else {
                    series.push({ 'name': '', 'value': '', 'colorValue': '' });
                }
                this.chartOptions.colorAxis = {
                    type: 'logarithmic',
                    minColor: '#EFEFFF',
                    maxColor: '#000022',
                    stops: [[0, '#EFEFFF'], [0.67, '#4444FF'], [1, '#000022']],
                    gridLineColor: "white",
                    showFirstLabel: false

                };
                this.chartOptions.legend.adjustChartSize = true;
                this.chartOptions.legend.itemStyle = {
                    font: '9pt Open Sans, arial, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                };
                this.chartOptions.series = [{
                    turboThreshold: 10000,
                    layoutAlgorithm: 'squarified',
                    data: series,
                    type: 'treemap'
                }];
            }
        }
    }, {
        key: 'getOptions',
        value: function getOptions() {
            return this.chartOptions;
        }
    }, {
        key: 'blendColors',
        value: function blendColors(c0, c1, p) {
            var f = parseInt(c0.slice(1), 16);
            var t = parseInt(c1.slice(1), 16);
            var R1 = f >> 16;
            var G1 = f >> 8 & 0x00FF;
            var B1 = f & 0x0000FF;
            var R2 = t >> 16;
            var G2 = t >> 8 & 0x00FF;
            var B2 = t & 0x0000FF;
            return "#" + (0x1000000 + (Math.round((R2 - R1) * p) + R1) * 0x10000 + (Math.round((G2 - G1) * p) + G1) * 0x100 + (Math.round((B2 - B1) * p) + B1)).toString(16).slice(1);
        }
    }, {
        key: 'percentForColor',
        value: function percentForColor(maxValue, relativeValue) {
            if (relativeValue < 0) {
                relativeValue = 0;
            }
            if (maxValue === 0) {
                maxValue = maxValue + 0.001;
            }
            var percent = 100 / maxValue * relativeValue / 100;
            return percent;
        }
    }]);

    return HeatMap;
}(Chart);

module.exports = HeatMap;