'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by 12072 on 8/9/16.
 */
var Chart = require('./Chart');
var _ = require('underscore');

var Line = function (_Chart) {
    _inherits(Line, _Chart);

    function Line(props) {
        _classCallCheck(this, Line);

        var _this = _possibleConstructorReturn(this, (Line.__proto__ || Object.getPrototypeOf(Line)).call(this, props));

        _this.chartOptions["plotOptions"]["series"] = { "stacking": null };
        _this.chartOptions["plotOptions"]["series"]["marker"] = { "symbol": 'circle' };
        _this.chartOptions["plotOptions"]["pie"]["dataLabels"] = { enabled: false };

        var widget = props.widget;
        if (widget.isDateComparison) _this.setRangeOptions(widget);else _this.setOptions(widget);

        _this.exportCSV = _this.exportCSV.bind(_this);
        return _this;
    }

    _createClass(Line, [{
        key: 'setOptions',
        value: function setOptions(linedata) {
            if (!(typeof linedata == "undefined")) {
                var series = [],
                    metricsList = linedata.metrics,
                    data_points = linedata.widgetData.dataPoints,
                    stepNo = 1,
                    count = 0,
                    opp = true,
                    displayNamesList = [],
                    metricAxisMap = {};

                for (var i = 0; i < metricsList.length; i++) {
                    metricAxisMap[metricsList[i].name] = i;
                }

                if (!(typeof data_points == 'undefined')) {
                    _.each(data_points, function (value, index) {
                        //showing display Name
                        var searchObj = _.first(_.where(metricsList, { name: value['name'] })),
                            displayName = searchObj ? searchObj.displayName : value['name'],
                            subName = value["dimensionValue"] ? " - " + value["dimensionValue"] : "",
                            finalDisplayName = displayName + subName;
                        displayNamesList.push(finalDisplayName);
                        series.push({
                            'name': finalDisplayName,
                            'data': value['data'],
                            'yAxis': metricAxisMap[searchObj.name],
                            'zoneAxis': value['zoneAxis'],
                            'zones': value['zones']
                        });
                    });
                    count = data_points[0].data.length;
                } else {
                    series.push({ 'name': '', 'data': '' });
                }

                var yaxisobjs = [],
                    opp = true,
                    metricsarray = [],
                    metricsarray = linedata.widgetData.metrics.split(','),
                    onlyOneYAxis = false;

                if (linedata.widgetData.dimensions) {
                    var dimArray = linedata.widgetData.dimensions.split(',');
                    if (dimArray.length > 1 && metricsarray.length == 1) {
                        onlyOneYAxis = true;
                    }
                }

                for (var i = 0; i < displayNamesList.length; i++) {
                    if (onlyOneYAxis) {
                        opp = false;
                    } else {
                        opp = !opp;
                    }
                    var a = {
                        labels: {
                            formatter: function formatter() {
                                return this.axis.defaultLabelFormatter.call(this);
                                //return this.value;
                            },
                            style: {
                                fontSize: '9px'
                            },
                            align: opp ? 'left' : 'right',
                            x: 0,
                            y: -2
                        },
                        title: {
                            text: " "
                        },
                        opposite: opp,
                        showEmpty: true,
                        offset: 0,
                        gridLineColor: '#EFEFEF'
                    };
                    if (i != 0) {
                        a["gridLineWidth"] = 0;
                    }
                    yaxisobjs.push(a);
                }
                this.chartOptions.yAxis = yaxisobjs;
                this.chartOptions.xAxis.categories = linedata.widgetData.categories;
                this.chartOptions.xAxis.crosshair = true;
                //var legend_text = _.pluck(linedata.dimensions, 'name').join(', ') + '<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>';
                this.chartOptions.chart.type = 'line';

                this.chartOptions.xAxis.type = 'datetime';
                if (count > 10) {
                    count = count + 3;
                    stepNo = Math.ceil(count / 10);
                }
                this.chartOptions.legend.labelFormatter = function () {
                    return this.name;
                };
                this.chartOptions.legend.adjustChartSize = true;
                this.chartOptions.legend.itemStyle = {
                    font: '9pt Open Sans, arial, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                };
                this.chartOptions.legend.symbolHeight = 8;
                this.chartOptions.legend.symbolWidth = 8;
                this.chartOptions.legend.symbolRadius = 6;

                this.chartOptions.xAxis.tickInterval = stepNo;
                this.chartOptions.tooltip = {
                    formatter: function formatter() {
                        var points = _.sortBy(this.points, function (point) {
                            return -point.y;
                        });
                        return '<span style="font-size:10px">' + this.x + '</span>' + '<table>' + points.map(function (point) {
                            return '<tr>' + '<td style="color:' + point.color + ';padding:0">' + point.series.name + ': </td>' + '<td style="padding:0"><b>' + point.y.toFixed(2) + '</b></td>' + '</tr>';
                        }).join("") + '</table>';
                    },
                    shared: true,
                    useHTML: true
                };
                this.chartOptions.series = series;
            }
        }
    }, {
        key: 'setRangeOptions',
        value: function setRangeOptions(linedata) {
            var _this2 = this;

            if (!(typeof linedata == "undefined")) {
                var series = [],
                    metricsList = linedata.metrics,
                    data_points = linedata.widgetData.dataPoints,
                    stepNo = 1,
                    count = 0,
                    opp = true,
                    displayNamesList = [],
                    metricAxisMap = {};

                for (var i = 0; i < metricsList.length; i++) {
                    metricAxisMap[metricsList[i].name] = i;
                }

                if (typeof data_points != 'undefined') {
                    var CompDateRange = "",
                        DateRange = "";
                    var options = { weekday: 'narrow', year: 'numeric', month: 'short', day: 'numeric' };
                    if (linedata.isDateComparison) {
                        DateRange = helper.dateChange(new Date(parseInt(linedata.fromTime)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(linedata.toTime)).toLocaleString('en-US', options));
                        CompDateRange = helper.dateChange(new Date(parseInt(linedata.fromTime1)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(linedata.toTime1)).toLocaleString('en-US', options));
                    }
                    if (typeof data_points.dateRange1 != 'undefined') {
                        _.each(data_points.dateRange1, function (item, index) {
                            var searchObj = _.first(_.where(metricsList, { name: item['name'] })),
                                displayName = searchObj ? searchObj.displayName : item['name'],
                                subName = item["dimensionValue"] ? " - " + item["dimensionValue"] : "",
                                finalDisplayName = displayName + subName;

                            displayNamesList.push(finalDisplayName);
                            series.push({
                                'name': finalDisplayName,
                                'data': item.data,
                                'color': _this2.comparisionColors[index],
                                'yAxis': metricAxisMap[searchObj.name]
                            });
                            if (typeof data_points.dateRange2[index] != 'undefined') {
                                series.push({
                                    'name': finalDisplayName,
                                    'data': data_points.dateRange2[index].data,
                                    'color': _this2.getColor[index],
                                    'yAxis': metricAxisMap[searchObj.name]
                                });
                            }
                        });
                        count = data_points.dateRange1[0].data.length;
                    }
                } else {
                    series.push({ 'name': '', 'data': '' });
                }
                if (linedata.isDateComparison) {
                    DateRange = helper.dateChange(new Date(parseInt(linedata.fromTime)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(linedata.toTime)).toLocaleString('en-US', options));
                    CompDateRange = helper.dateChange(new Date(parseInt(linedata.fromTime1)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(linedata.toTime1)).toLocaleString('en-US', options));
                }

                this.chartOptions.legend.labelFormatter = function () {
                    if (this.index % 2 == 0) return this.name + " - " + DateRange;else return this.name + " - " + CompDateRange;
                };
                var yaxisobjs = [],
                    opp = true,
                    metricsarray = [],
                    onlyOneYAxis = false;
                metricsarray = linedata.widgetData.metrics.split(',');

                if (linedata.widgetData.dimensions) {
                    var dimArray = linedata.widgetData.dimensions.split(',');
                    if (dimArray.length > 1 && metricsarray.length == 1) {
                        onlyOneYAxis = true;
                    }
                }

                for (var i = 0; i < displayNamesList.length; i++) {
                    if (onlyOneYAxis) {
                        opp = false;
                    } else {
                        opp = !opp;
                    }
                    var a = {
                        labels: {
                            formatter: function formatter() {
                                return this.axis.defaultLabelFormatter.call(this);
                            },
                            align: opp ? 'left' : 'right',
                            x: 0,
                            y: -2
                        },
                        style: {
                            fontSize: '9px'
                        },
                        title: {
                            text: " "
                        },
                        opposite: !opp,
                        showEmpty: true,
                        offset: 0
                    };
                    if (i != 0) {
                        a["gridLineWidth"] = 0;
                    }
                    yaxisobjs.push(a);
                }
                this.chartOptions.yAxis = yaxisobjs;
                this.chartOptions.xAxis.categories = linedata.widgetData.categories.dateRange1;
                this.chartOptions.xAxis.crosshair = true;
                var legend_text = _.pluck(linedata.dimensions, 'name').join(', ') + '<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>';
                this.chartOptions.chart.type = 'line';
                this.chartOptions.xAxis.type = 'datetime';
                if (count > 10) {
                    count = count + 3;
                    stepNo = Math.ceil(count / 10);
                }
                this.chartOptions.xAxis.tickInterval = stepNo;
                this.chartOptions.legend.adjustChartSize = true;

                this.chartOptions.legend.itemStyle = {
                    font: '9pt Trebuchet MS, Verdana, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                };
                this.chartOptions.tooltip = {
                    formatter: function formatter() {
                        var points = _.sortBy(this.points, function (point) {
                            return -point.y;
                        });
                        return '<span style="font-size:10px">' + this.x + '</span>' + '<table>' + points.map(function (point) {
                            return '<tr>' + '<td style="color:' + point.color + ';padding:0">' + point.series.name + ': </td>' + '<td style="padding:0"><b>' + point.y.toFixed(2) + '</b></td>' + '</tr>';
                        }).join("") + '</table>';
                    },
                    shared: true,
                    useHTML: true
                };
                // this.chartOptions.tooltip = {
                //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:,.0f}</b></td></tr>',
                //     footerFormat: '</table>', shared: true, useHTML: true
                // };
                this.chartOptions.series = series;
            }
        }
    }, {
        key: 'getOptions',
        value: function getOptions() {
            return this.chartOptions;
        }
    }, {
        key: 'exportCSV',
        value: function exportCSV(widget, filename) {
            if (!widget.isDateComparison) {
                this.exportCSVNormal(widget, filename);
            } else {
                this.exportCSVCompareWith(widget, filename);
            }
        }
    }, {
        key: 'exportCSVNormal',
        value: function exportCSVNormal(widget, filename) {
            var metrics = widget.metrics,
                dimensions = widget.dimensions,
                widgetData = widget.widgetData;

            var dimensionFlags = {},
                metricFlags = {};

            var dimensionDisplayName = dimensions.map(function (item, i) {
                dimensionFlags[item.name] = i + 1;
                return item.displayName;
            });
            var metricDisplayName = metrics.map(function (item, i) {
                metricFlags[item.name] = i + dimensions.length + 1;
                return item.displayName;
            });
            dimensionDisplayName.splice(0, 0, "Date");
            var header = common.headerToCSV(dimensionDisplayName.concat(metricDisplayName));

            var csvdata = '';
            csvdata = csvdata + header + '\r\n';
            var array = [];

            var data = _.chain(widgetData.dataPoints).groupBy("dimensionValue").value();

            for (var i = 0; i < Object.keys(data).length; i++) {
                var dimensionValue = Object.keys(data)[i];
                for (var j = 0; j < widgetData.categories.length; j++) {
                    var obj = new Array(dimensions.length + metrics.length + 1);
                    for (var k = 0; k < data[dimensionValue].length; k++) {
                        var indexMetric = metricFlags[data[dimensionValue][k].name];
                        obj[0] = widgetData.categories[j];
                        obj[indexMetric] = data[dimensionValue][k].data[j];
                        if (dimensionDisplayName.length > 1) {
                            obj[1] = dimensionValue;
                        }
                    }
                    array.push(obj);
                }
            }
            csvdata = csvdata + common.json2csv(array);
            common.opencsv(csvdata, filename);
        }
    }, {
        key: 'exportCSVCompareWith',
        value: function exportCSVCompareWith(widget, filename) {
            var metrics = widget.metrics,
                dimensions = widget.dimensions,
                widgetData = widget.widgetData;

            var dimensionFlags = {},
                metricFlags = {};

            var dimensionDisplayName = dimensions.map(function (item, i) {
                dimensionFlags[item.name] = i + 3;
                return item.displayName;
            });
            var metricDisplayName = metrics.map(function (item, i) {
                metricFlags[item.name] = i + dimensions.length + 3;
                return item.displayName;
            });
            dimensionDisplayName.splice(0, 0, "Start Date");
            dimensionDisplayName.splice(1, 0, "End Date");
            dimensionDisplayName.splice(2, 0, "Date");

            var header = common.headerToCSV(dimensionDisplayName.concat(metricDisplayName));
            var csvdata = '';
            csvdata = csvdata + header + '\r\n';
            var array1 = [];
            var array2 = [];

            var fromTime = moment(parseInt(widget.fromTime)).format('MMM DD YYYY');
            var fromTime1 = moment(parseInt(widget.fromTime1)).format('MMM DD YYYY');
            var toTime = moment(parseInt(widget.toTime)).format('MMM DD YYYY');
            var toTime1 = moment(parseInt(widget.toTime1)).format('MMM DD YYYY');

            for (var i = 0; i < Object.keys(widgetData.dataPoints).length; i++) {
                var dateRange = Object.keys(widgetData.dataPoints)[i];
                if (dateRange != 'percentChange') {
                    var startDate = dateRange == "dateRange1" ? fromTime : fromTime1;
                    var endDate = dateRange == "dateRange1" ? toTime : toTime1;
                    var array = dateRange == "dateRange1" ? array1 : array2;
                    var data = _.chain(widgetData.dataPoints[dateRange]).groupBy("dimensionValue").value();
                    for (var j = 0; j < Object.keys(data).length; j++) {
                        var dimensionValue = Object.keys(data)[j];
                        for (var k = 0; k < widgetData.categories[dateRange].length; k++) {
                            var obj = new Array(dimensions.length + metrics.length + 1);
                            for (var l = 0; l < data[dimensionValue].length; l++) {
                                var indexMetric = metricFlags[data[dimensionValue][l].name];
                                obj[0] = startDate;
                                obj[1] = endDate;
                                obj[2] = widgetData.categories[dateRange][k];
                                obj[indexMetric] = data[dimensionValue][l].data[k];
                                if (dimensionDisplayName.length > 3) {
                                    obj[3] = dimensionValue;
                                }
                            }
                            array.push(obj);
                        }
                    }
                }
            }
            csvdata = csvdata + common.json2csv(array1.concat(array2));
            common.opencsv(csvdata, filename);
        }
    }]);

    return Line;
}(Chart);

module.exports = Line;