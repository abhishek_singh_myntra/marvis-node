'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by 12072 on 8/9/16.
 */
var Chart = require('./Chart');
var _ = require('underscore');

var Pie = function (_Chart) {
    _inherits(Pie, _Chart);

    function Pie(props) {
        _classCallCheck(this, Pie);

        var _this = _possibleConstructorReturn(this, (Pie.__proto__ || Object.getPrototypeOf(Pie)).call(this, props));

        _this.chartOptions["plotOptions"]["series"] = { "stacking": null };

        var widget = props.widget;
        _this.setOptions(widget);

        _this.overrideChartOptionsByWidgetType(widget, props.pieOptions);

        return _this;
    }

    _createClass(Pie, [{
        key: 'overrideChartOptionsByWidgetType',
        value: function overrideChartOptionsByWidgetType(piedata) {
            var pieOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            this.chartOptions = _.extend(this.chartOptions, pieOptions);
        }
    }, {
        key: 'setOptions',
        value: function setOptions(piedata) {
            var series = [];
            if (!(typeof piedata == "undefined")) {
                var legend = _.pluck(piedata.metrics, 'displayName').join(', ') + ' by ' + _.pluck(piedata.dimensions, 'displayName').join(', ');
                this.chartOptions.chart.type = 'pie'; // check default value of this
                this.chartOptions.title = {
                    text: legend,
                    align: 'center',
                    verticalAlign: 'bottom',
                    y: 5,
                    style: {
                        fontSize: "1rem",
                        color: "#a09797"
                    }
                };
                this.chartOptions.tooltip = {
                    headerFormat: '<span style="font-size:10px"> {series.name} : {point.key}</span><br />',
                    pointFormat: '<span style="font-size:10px">  {point.x} : <b>{point.y:,.0f}</b></span><br />',
                    footerFormat: '<span style="font-size:10px"> Percentage : <b>{point.percentage:,.1f}% </b></span>', shared: true, useHTML: false
                };
                this.chartOptions.plotOptions.pie = {
                    allowPointSelect: true,
                    cursor: 'cursor',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                };
                this.chartOptions.showInLegend = true;
                this.chartOptions.series.type = 'pie';
                this.chartOptions.xAxis.crosshair = false;

                if (!(typeof piedata.widgetData.data == 'undefined')) {
                    _.each(piedata.widgetData.data, function (value) {
                        series.push({ 'name': value['name'], 'y': parseFloat(value['data'][0]), 'x': _.pluck(piedata.metrics, 'displayName').join(', ') });
                    });
                } else {
                    series.push({ 'name': '', 'y': '' });
                }
                this.chartOptions.legend.labelFormatter = function () {
                    return this.name;
                };
                this.chartOptions.legend.adjustChartSize = true;
                this.chartOptions.legend.itemStyle = {
                    font: '9pt Open Sans, arial, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                };
                this.chartOptions.legend.symbolHeight = 8;
                this.chartOptions.legend.symbolWidth = 8;
                this.chartOptions.legend.symbolRadius = 6;

                this.chartOptions.series = [{
                    name: _.first(piedata.dimensions).displayName,
                    colorByPoint: true,
                    data: series,
                    size: '100%',
                    innerSize: '50%'

                }];
                // for lables
                this.chartOptions["plotOptions"]["pie"]["dataLabels"] = {
                    enabled: false,
                    formatter: function formatter() {
                        return Math.round(this.percentage * 100) / 100 + ' %';
                    }
                };
            }
        }
    }, {
        key: 'setRangeOptions',
        value: function setRangeOptions(piedata) {
            var series = [],
                compseries = [];
            if (!(typeof piedata == "undefined")) {
                //var legend = 'Share of ' + _.pluck(piedata.dimensions, 'name').join(', ') + " measured in " + _.pluck(piedata.metrics, 'name').join(', ');
                //legend = legend + '<br/><span style="font-size: 9px; color: #666; font-weight: normal">(Click to hide)</span>';
                this.chartOptions.chart.type = 'pie'; // check default value of this
                this.chartOptions.xAxis.crosshair = false;
                this.chartOptions.legend.itemStyle = {
                    font: '9pt Trebuchet MS, Verdana, sans-serif',
                    color: 'black',
                    fontWeight: 'normal'
                };
                this.chartOptions.tooltip = {
                    headerFormat: '<span style="font-size:10px"> {series.name} : {point.key}</span><br />',
                    pointFormat: '<span style="font-size:10px">  {point.x} : <b>{point.y:,.0f}</b></span><br />',
                    footerFormat: '<span style="font-size:10px"> Percentage : <b>{point.percentage:,.1f}% </b></span>', shared: true, useHTML: false
                };
                this.chartOptions.series.type = 'pie';
                this.chartOptions.plotOptions.series = {
                    point: {
                        events: {
                            legendItemClick: function legendItemClick(event) {
                                var _this2 = this;

                                var visibility = this.visible ? true : false;
                                var legendItem = this.name;
                                event.stopPropagation();
                                event.preventDefault();
                                _.each(this.series.chart.series, function (f, i) {
                                    var count = 0;
                                    _.each(_this2.data, function (z, k) {
                                        if (z.name == legendItem) {
                                            if (z.visible && count == 0) {
                                                count++;
                                                z.setVisible(false);
                                            } else {
                                                count++;
                                                z.setVisible(true);
                                            }
                                        }
                                    });
                                });
                            }
                        }
                    }
                };
                var CompDateRange = "",
                    DateRange = "";
                var options = { weekday: 'narrow', year: 'numeric', month: 'short', day: 'numeric' };

                if (typeof piedata.widgetData.dateRange1 != 'undefined') {
                    _.each(piedata.widgetData.dateRange1, function (item) {
                        series.push({ 'name': item.name, 'y': item.data[0], 'x': _.pluck(piedata.metrics, 'displayName').join(', ') });
                    });
                } else {
                    series.push({ 'name': '', 'y': 0 });
                }
                if (!(typeof piedata.widgetData.dateRange2 == 'undefined')) {
                    _.each(piedata.widgetData.dateRange2, function (item) {
                        compseries.push({ 'name': item.name, 'y': item.data[0], 'x': _.pluck(piedata.metrics, 'displayName').join(', ') });
                    });
                } else {
                    compseries.push({ 'name': '', 'y': 0 });
                }
                if (!(typeof piedata.widgetData.dateRange1 == 'undefined')) {
                    this.chartOptions.legend.labelFormatter = function () {
                        return this.name;
                    };
                }
                this.chartOptions.series = [{
                    name: _.first(piedata.dimensions).displayName,
                    colorByPoint: true,
                    data: series,
                    center: [null, '25%'],
                    size: '40%',
                    showInLegend: false,
                    title: {
                        align: 'center',
                        text: '<b>' + DateRange + '</b>',
                        verticalAlign: 'top',
                        y: -40,
                        useHTML: true
                    },
                    innerSize: '50%'
                }, {
                    name: _.first(piedata.dimensions).displayName,
                    colorByPoint: true,
                    data: compseries,
                    center: [null, '85%'],
                    size: '40%',
                    showInLegend: true,
                    title: {
                        align: 'center',
                        text: '<b>' + CompDateRange + '</b>',
                        verticalAlign: 'top',
                        y: -40,
                        useHTML: true
                    },
                    innerSize: '50%'
                }];
                //for lables
                this.chartOptions["plotOptions"]["pie"]["dataLabels"] = {
                    enabled: false,
                    formatter: function formatter() {
                        return Math.round(this.percentage * 100) / 100 + ' %';
                    }
                };
            }
        }
    }, {
        key: 'getOptions',
        value: function getOptions() {
            return this.chartOptions;
        }
    }]);

    return Pie;
}(Chart);

module.exports = Pie;