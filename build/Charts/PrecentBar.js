'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by 12072 on 8/17/16.
 */
var Bar = require('./Bar');

var PrecentBar_ = function (_Bar) {
    _inherits(PrecentBar_, _Bar);

    function PrecentBar_(props) {
        _classCallCheck(this, PrecentBar_);

        var _this = _possibleConstructorReturn(this, (PrecentBar_.__proto__ || Object.getPrototypeOf(PrecentBar_)).call(this, props));

        _this.chartOptions.plotOptions.series.stacking = 'percent';
        return _this;
    }

    _createClass(PrecentBar_, [{
        key: 'getOptions',
        value: function getOptions() {
            return this.chartOptions;
        }
    }]);

    return PrecentBar_;
}(Bar);

module.exports = PrecentBar_;