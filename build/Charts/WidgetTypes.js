'use strict';

var Bar = require('./Bar');
var Line = require('./Line');
var Pie = require('./Pie');
var Gauge = require('./Gauge');
var Heatmap = require('./HeatMap');
var Stackbar = require('./StackBar');
var Percentbar = require('./PrecentBar');
var Flatbar = require('./FlatBar');

module.exports = {
    Bar: Bar,
    Line: Line,
    Pie: Pie,
    Gauge: Gauge,
    Flatbar: Flatbar,
    Heatmap: Heatmap,
    Stackbar: Stackbar,
    Percentbar: Percentbar
};