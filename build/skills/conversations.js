'use strict';

/*

WHAT IS THIS?

This module demonstrates simple uses of Botkit's conversation system.

In this example, Botkit hears a keyword, then asks a question. Different paths
through the conversation are chosen based on the user's response.

*/

var WidgetTypes = require('../Charts/WidgetTypes');
var superAgent = require('superagent');
var Redis = require('ioredis');
var redis = new Redis();

module.exports = function (controller) {

    controller.hears(['.*'], ['direct_message', 'direct_mention', 'interactive'], function (speech, message) {
        console.log(message);
        processMessage(speech, message);
    });
};

function processMessage(speech, message) {
    //http://marvis-test-service.mynt.myntra.com/marvis-api/rest/v1/nlp/?message=
    //http://localhost:8080/rest/v1/nlp?message=

    console.log('Getting nlp for', message.text);

    superAgent.get('http://localhost:8080/rest/v1/nlp?message=' + message.text).set('user-email', 'jarvis@myntra.com').set('Content-Length', '0').end(function (error, response) {

        if (error) {
            console.log(error, response);
            return speech.replyWithTyping(message, "Oops didn't get you");
        } else {
            nlpData = JSON.parse(response.text);
            nlpData = nlpData[0];

            if (!nlpData) {
                return speech.replyWithTyping(message, "No match found");
            } else if (nlpData.metrics.length === 0 && nlpData.dimensions.length === 0) {
                return redis.get(message.user + '_' + message.channel, function (err, data) {
                    console.log('\n \n Data from redis', data);
                    if (!err && data && nlpData.widgetType) {
                        var postData = JSON.parse(data);
                        postData.widgetType = nlpData.widgetType;
                        return sendScreenshot(postData, speech, message);
                    } else {
                        return speech.replyWithTyping(message, "No matches. Please re-phrase your request.");
                    }
                });
            }

            persistUserQuery(message.user, message.channel, '');

            if (nlpData.metrics.length === 0) {
                return speech.replyWithTyping(message, "No metrics matched. Please re-phrase your request.");
            } else if (!nlpData.fromTime || !nlpData.toTime) {
                return speech.replyWithTyping(message, "Sorry you provided insufficient dates in your message");
            } else if (nlpData.suggestions && nlpData.suggestions.length) {
                return sendSuggestions(speech, message, nlpData.suggestions);
            }
            var postData = getPOSTData(nlpData);
            return sendScreenshot(postData, speech, message);
        }
    });
}

function sendSuggestions(speech, message, suggestions) {
    speech.startConversation(message, function (err, convo) {
        convo.ask({
            attachments: [{
                title: 'Ambiguous statement. Did you mean?',
                callback_id: '123',
                attachment_type: 'default',
                actions: suggestions.map(function (suggestion) {
                    return {
                        name: suggestion.displayName,
                        text: suggestion.displayName,
                        value: suggestion.value,
                        type: 'button',
                        style: 'primary'
                    };
                })
            }]
        }, [{
            pattern: '.*',
            callback: function callback(reply, convo) {
                processMessage(speech, reply);
            }
        }]);
    });
}

function sendScreenshot(postData, speech, message) {

    console.log(postData);

    superAgent.post('http://udp-fe.mynt.myntra.com/myntra/udp/explore').send(postData).set('user-email', 'jarvis@myntra.com').end(function (error, response) {
        var json = {},
            str = '';

        if (error || !response || response.status != 200) return speech.replyWithTyping(message, "No data found or server issues. " + response.status);

        persistUserQuery(message.user, message.channel, JSON.stringify(postData));

        try {
            json = JSON.parse(response.text);
        } catch (e) {
            console.log(e);
        }
        var widgetType = postData.widgetType.toLowerCase();
        widgetType = widgetType.charAt(0).toUpperCase() + widgetType.slice(1);
        var Widget = WidgetTypes[widgetType];

        if (Widget) {

            var options = void 0;

            try {
                options = JSON.stringify(new Widget({ widget: json }).getOptions());
            } catch (e) {}

            console.log('options', options);

            if (!options) return speech.replyWithTyping(message, "Some error occurred.");

            superAgent.post('https://export.highcharts.com/').send({
                "infile": options,
                "width": false,
                "scale": false,
                "constr": "Chart",
                "styledMode": false,
                "type": "image/png",
                "asyncRendering": false,
                "async": true
            }).end(function (errorIm, resultImg) {
                console.log("resultImg", resultImg.text, "https://export.highcharts.com/" + resultImg.text);
                speech.replyWithTyping(message, "https://export.highcharts.com/" + resultImg.text);
            });
        } else {
            if (json.widgetData) {
                speech.replyWithTyping(message, "`" + json.widgetData["metricName"] + ":" + json.widgetData["metricValue"].toLocaleString() + "`");
            } else {
                speech.replyWithTyping(message, "No data found");
            }
        }
    });
}

function getPOSTData(parsedJson) {
    console.log(parsedJson);
    return {
        widgetType: parsedJson.widgetType || 'metric',
        reportName: parsedJson.reportName,
        fetchData: true,
        dimensionFilters: parsedJson.dimensionFilters.map(function (dimensionFilter) {
            return getDimensionFilter(dimensionFilter);
        }),
        metricFilters: [],
        metrics: parsedJson.metrics ? parsedJson.metrics.map(function (metric) {
            return getMetric(metric);
        }) : [],
        dimensions: parsedJson.dimensions ? parsedJson.dimensions.map(function (dimension) {
            return getDimension(dimension);
        }) : [],
        fromTime: parsedJson.fromTime,
        toTime: parsedJson.toTime,
        limit: 8
    };
}

function getDimensionFilter(dimension) {
    return {
        field: {
            name: dimension.dim_name
        },
        includeCondition: "show",
        condition: "in",
        cvalue: dimension.dim_values.join(","),
        dimension: dimension.dim_name
    };
}

function getMetricFilter(metric) {}

function getMetric(metric) {
    return {
        "name": metric,
        "sortBy": "desc"
    };
}

function getDimension(dimension) {
    return {
        "name": dimension
    };
}

function eachKey(object, callback) {
    Object.keys(object).forEach(function (key) {
        callback(key, object[key]);
    });
}

function persistUserQuery(user, channel, data) {
    redis.set(user + '_' + channel, JSON.stringify(data));
}