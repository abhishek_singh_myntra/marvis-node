'use strict';

module.exports = function (controller) {

    // create special handlers for certain actions in buttons
    // if the button action is 'action', trigger an event
    // if the button action is 'say', act as if user said that thing
    controller.on('interactive_message_callback', function (bot, message) {
        console.log('Message action triggered', message.actions[0].value);
    });
};