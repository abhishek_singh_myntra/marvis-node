/**
 * Created by 12072 on 8/9/16.
 */

const Chart = require('./Chart');
var _ = require('underscore');

class Bar extends Chart {

    constructor(props) {
        super(props);
        this.chartOptions["plotOptions"]["pie"]["dataLabels"] = {enabled: false};

        let widget = props.widget;
        if(widget.isDateComparison)
            this.setRangeOptions(widget);
        else
            this.setOptions(widget);
        this.chartOptions.plotOptions.series.stacking = null;
    }

    setOptions(bardata) {
        if (!(typeof (bardata) == "undefined")) {
            this.chartOptions.chart.type = 'column';
            if (!(typeof (bardata.widgetData) == "undefined") && (bardata.widgetData != null)) {
                var series = [], data_points = bardata.widgetData.dataPoints, metricsList = bardata.metrics;
                if (!(typeof (data_points) == 'undefined')) {
                    _.each(data_points,  (value) => {
                        var searchObj=_.first(_.where(metricsList,{name:value['name']})),
                            displayName=(searchObj)?searchObj.displayName:value['name'];
                        const data = value['data'].map((item, i) => {
                            return {
                                color: data_points.length === 1 ? this.chartOptions.colors[i % 10] : null,
                                y: parseFloat(item)
                            }
                        })
                        series.push({ 'name': displayName, data});
                    });
                } else {
                    series.push({ 'name': '', 'data': '' });
                }


                this.chartOptions.legend.labelFormatter = function() {
                    return this.name;
                };
                this.setCommonOptions();
                this.chartOptions.xAxis.categories = bardata.widgetData.categories;
                this.chartOptions.series = series;

                if (data_points && data_points.length === 1) {
                    this.chartOptions.legend.symbolHeight = 0 
                    this.chartOptions.legend.symbolWidth = 0
                    this.chartOptions.legend.symbolRadius = 0
                }

            }
            // console.log(this.chartOptions)
        }
    }

    setRangeOptions(bardata) {
        if (typeof (bardata) != 'undefined') {
            this.chartOptions.chart.type = 'column';
            if (!(typeof (bardata.widgetData) == "undefined") && (bardata.widgetData != null)) {
                var series = [], data_points = bardata.widgetData.dataPoints,metricsList=bardata.metrics;
                if (typeof data_points != 'undefined') {
                    if (typeof (data_points.dateRange1) != 'undefined') {
                        _.each(data_points.dateRange1, (item, index) => {
                            var searchObj=_.first(_.where(metricsList,{name:item['name']})),
                                displayName=(searchObj)?searchObj.displayName:item['name'];
                            series.push({ 'cname': item.name + " " + DateRange, 'name': displayName, 'data': item.data.map(parseFloat),'stack':"range1",'color': this.comparisionColors[index] });
                            if (typeof data_points.dateRange2[index] != 'undefined')
                                series.push({ 'cname': data_points.dateRange2[index].name + " " + CompDateRange,'stack':"range2", 'name': displayName, 'data': data_points.dateRange2[index].data.map(parseFloat), 'color': this.getColor[index] });
                        });
                    }
                }
                else {
                    series.push({ 'name': '', 'data': '' });
                }
                var CompDateRange = "", DateRange = "";
                var options = { weekday: 'narrow', year: 'numeric', month: 'short', day: 'numeric' };
                if (bardata.isDateComparison) {
                    DateRange = helper.dateChange(new Date(parseInt(bardata.fromTime)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(bardata.toTime)).toLocaleString('en-US', options));
                    CompDateRange = helper.dateChange(new Date(parseInt(bardata.fromTime1)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(bardata.toTime1)).toLocaleString('en-US', options));
                }

                this.chartOptions.legend.labelFormatter = function () {
                    if (this.index % 2 == 0)
                        return this.name + DateRange;
                    else
                        return this.name + CompDateRange;
                }
                this.setCommonOptions();
                this.chartOptions.xAxis.categories = bardata.widgetData.categories;
                this.chartOptions.series = series;
            }
        }
    }

    setCommonOptions(){
        this.chartOptions.legend.adjustChartSize = true;
        this.chartOptions.legend.itemStyle = {
            font: '9pt Open Sans, arial, sans-serif',
            color: 'black',
            fontWeight: 'normal'
        };
        this.chartOptions.tooltip = {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:,.2f} </b></td></tr>',
            footerFormat: '</table>', shared: true, useHTML: true
        },
        this.chartOptions.xAxis.crosshair = true;
        this.chartOptions.yAxis.labels = {
            formatter: function () {
                return this.axis.defaultLabelFormatter.call(this);
            },
            style: {
                fontSize: '9px'
            },
            x: 0,
            y: -2
        };
        this.chartOptions.yAxis.gridLineColor = '#EFEFEF';
        //for lables
        this.chartOptions["plotOptions"]["series"]["dataLabels"]={
            enabled: true,
            formatter: this.labelFormatter
        };

        this.chartOptions.legend.symbolHeight= 8;
        this.chartOptions.legend.symbolWidth= 8;
        this.chartOptions.legend.symbolRadius= 6;

        this.chartOptions.xAxis.tickInterval = 0;
    }

    labelFormatter(){
        var num = this.y;
        var isNegative = false;
        var formattedNumber;
        if (num < 0) {
            isNegative = true
        }
        num = Math.abs(num)
        if (num >= 1000000000) {
            formattedNumber = (num / 1000000000).toFixed(2).replace(/\.0$/, '') + 'G';
        } else if (num >= 1000000) {
            formattedNumber =  (num / 1000000).toFixed(2).replace(/\.0$/, '') + 'M';
        } else  if (num >= 1000) {
            formattedNumber =  (num / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
        } else {
            formattedNumber = num;
        }
        if(isNegative) { formattedNumber = '-' + formattedNumber }
        return formattedNumber;
    }

    getOptions() {
        return this.chartOptions;
    }

    exportCSV(widget, filename) {
        if (!widget.isDateComparison) {
            this.exportCSVNormal(widget, filename);
        } else {
            this.exportCSVCompareWith(widget, filename)
        }
    }

    exportCSVNormal(widget, filename) {
        const {metrics, dimensions, widgetData} = widget;
        const dimensionFlags = {}, metricFlags = {}

        let dimensionDisplayName = dimensions.map((item, i) => {
            dimensionFlags[item.name] = i
            return item.displayName
        });
        let metricDisplayName = metrics.map((item, i) => {
            metricFlags[item.name] = i + dimensions.length
            return item.displayName
        })
        let header = common.headerToCSV(dimensionDisplayName.concat(metricDisplayName));

        let csvdata ='';
        csvdata = csvdata + header + '\r\n';
        let array = [];

        for (let i = 0; i < widgetData.dataPoints.length; i++) {
            for (let j = 0 ; j < widgetData.categories.length ; j++) {
                var obj = array[j] ? array[j] : new Array(dimensions.length + metrics.length);
                obj[0] = widgetData.categories[j];
                const index = metricFlags[widgetData.dataPoints[i].name];
                obj[index] = widgetData.dataPoints[i].data[j];
                if (!array[j]) {
                    array.push(obj);
                }
            }
        }
        csvdata = csvdata + common.json2csv(array);
        common.opencsv(csvdata, filename)
    }

    exportCSVCompareWith(widget, filename) {
        const {metrics, dimensions, widgetData} = widget;
        const dimensionFlags = {}, metricFlags = {}

        let dimensionDisplayName = dimensions.map((item, i) => {
            dimensionFlags[item.name] = i +2
            return item.displayName
        });
        let metricDisplayName = metrics.map((item, i) => {
            metricFlags[item.name] = i + dimensions.length +2
            return item.displayName
        })
        dimensionDisplayName.splice(0, 0, "Start Date");
        dimensionDisplayName.splice(1, 0, "End Date");

        let header = common.headerToCSV(dimensionDisplayName.concat(metricDisplayName));
        let csvdata ='';
        csvdata = csvdata + header + '\r\n';
        let array1 = [];
        let array2 = [];

        const fromTime = moment(parseInt(widget.fromTime)).format('MMM DD YYYY');
        const fromTime1 = moment(parseInt(widget.fromTime1)).format('MMM DD YYYY') ;
        const toTime = moment(parseInt(widget.toTime)).format('MMM DD YYYY');
        const toTime1 = moment(parseInt(widget.toTime1)).format('MMM DD YYYY') ;

        for(let i = 0; i < Object.keys(widgetData.dataPoints).length; i++) {
            let dateRange = Object.keys(widgetData.dataPoints)[i];
            if(dateRange != 'percentChange') {
                let startDate = dateRange == "dateRange1" ? fromTime : fromTime1;
                let endDate = dateRange == "dateRange1" ? toTime : toTime1;
                let array = dateRange == "dateRange1" ? array1 : array2;
                let categories = _.uniq(widgetData.categories);
                _.each(widgetData.dataPoints[dateRange], function(value){
                    for (let k = 0 ; k < categories.length ; k++) {
                        var obj = array[k] ? array[k] : new Array(dimensions.length + metrics.length + 2);
                        obj[0] = startDate;
                        obj[1] = endDate;
                        obj[2] = categories[k];
                        const index = metricFlags[value.name];
                        obj[index] = value.data[k];
                        if (!array[k]) {
                            array.push(obj);
                        }
                    }
                })
            }
        }
        csvdata = csvdata + common.json2csv(array1.concat(array2));
        common.opencsv(csvdata, filename)
    }

}
module.exports = Bar;