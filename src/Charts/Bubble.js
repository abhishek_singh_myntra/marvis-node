/**
 * Created by 12072 on 8/9/16.
 */
var Chart = require('./Chart');
var _ = require('underscore');
var helper = require( '../../../../utils/helper');
var SmartChartWrapper = require ('./SmartChartWrapper');
var ErrorMessage = require('../ErrorMessage');
var common = require('../../../../utils/common');

class Bubble extends Chart {

    constructor(props) {
        super(props);
        let widget = props.widget;
        if(widget.isDateComparison)
            this.setRangeOptions(widget);
        else
            this.setOptions(widget);

        this.overrideChartOptionsByWidgetType();
        this.exportCSV = this.exportCSV.bind(this);

    }

    overrideChartOptionsByWidgetType() {
    }

    setOptions(bubblechartdata) {
        var series = [];
        if (!(typeof (bubblechartdata) == "undefined")) {
            let metrics=_.pluck(bubblechartdata.metrics, 'name');
            let metrics_displayName=_.pluck(bubblechartdata.metrics, 'displayName');
            var dimension_name=_.pluck(bubblechartdata.dimensions, 'name')[0];
            var legend = _.pluck(bubblechartdata.metrics, 'displayName').join(', ') + ' by ' + _.pluck(bubblechartdata.dimensions, 'displayName').join(', ');
            this.chartOptions.chart.type = 'bubble';
            this.chartOptions.tooltip={
                pointFormat: '<span style="font-size:10px">  <b>{point.dimension}</b>' +
                            '<br />'+ metrics_displayName[0] +': <b>{point.x}</b>' +
                            '<br />'+ metrics_displayName[1] +' : <b>{point.y}</b>' +
                            '<br />'+ metrics_displayName[2] +': <b>{point.z}</b></span><br />',
                shared: true,
                useHTML: true,
                headerFormat: `<span />`
            };
            this.chartOptions.xAxis={
                gridLineWidth: 1,
                title: {
                    text: metrics_displayName[0]
                },
                plotLines: [{
                    color: 'black',
                    dashStyle: 'dot',
                    width: 2,
                    label: {
                        rotation: 0,
                        y: 15,
                        style: {
                            fontStyle: 'italic'
                        },
                    },
                    zIndex: 3
                }]
            };
            this.chartOptions.yAxis={
                startOnTick: false,
                endOnTick: false,
                title: {
                    text: metrics_displayName[1]
                },
                maxPadding: 0.2,
                plotLines: [{
                    color: 'black',
                    dashStyle: 'dot',
                    width: 2,
                    label: {
                        align: 'right',
                        style: {
                            fontStyle: 'italic'
                        },
                        x: -10
                    },
                    zIndex: 3
                }]
            }
            this.chartOptions.plotOptions.series = {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            };
            this.chartOptions.showInLegend = true;
            this.chartOptions.series.type = 'bubble';
            this.chartOptions.xAxis.crosshair = false;

            if (!(typeof (bubblechartdata.widgetData.result) == 'undefined')) {
                _.each(bubblechartdata.widgetData.result, (value) => {
                        series.push({ 'dimension': value[dimension_name], 'x': parseFloat(value[metrics[0]].replace(/,/g , '')),'y': parseFloat(value[metrics[1]].replace(/,/g , '')) ,'z': parseFloat(value[metrics[2]].replace(/,/g , ''))});
                });
            }
            else {
                series.push({ 'dimension': '', 'x': '','y': '','z': '' });
            }
            this.chartOptions.legend.adjustChartSize = true;
            this.chartOptions.legend.itemStyle = {
                font: '9pt Open Sans, arial, sans-serif',
                color: 'black',
                fontWeight: 'normal',
                //width: parseInt(strwidth) - 30 // or whatever
            }
            this.chartOptions.legend.symbolHeight= 8;
            this.chartOptions.legend.symbolWidth= 8;
            this.chartOptions.legend.symbolRadius= 6;

            this.chartOptions.series = [{
                name: legend,
                data: series
            }];
        }
    }

    setRangeOptions(widget) {
        var options = { weekday: 'narrow', year: 'numeric', month: 'short', day: 'numeric' };
        var DateRange = helper.dateChange(new Date(parseInt(widget.fromTime)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(widget.toTime)).toLocaleString('en-US', options));
        var CompDateRange = helper.dateChange(new Date(parseInt(widget.fromTime1)).toLocaleString('en-US', options)) + " - " + helper.dateChange(new Date(parseInt(widget.toTime1)).toLocaleString('en-US', options));
        let metrics_displayName=_.pluck(widget.metrics, 'displayName');
        this.chartOptions.chart.type = 'bubble';
        var legend = _.pluck(widget.metrics, 'displayName').join(', ') + ' by ' + _.pluck(widget.dimensions, 'displayName').join(', ');

        this.chartOptions.tooltip={
            pointFormat: '<span style="font-size:10px"><b>  {point.dimension}</b>' +
            '<br />'+ metrics_displayName[0] +': <b>{point.x}</b>' +
            '<br />'+ metrics_displayName[1] +' : <b>{point.y}</b>' +
            '<br />'+ metrics_displayName[2] +': <b>{point.z}</b></span><br />',
            shared: true,
            useHTML: true,
            headerFormat: `<span />`
        };
        this.chartOptions.xAxis={
            gridLineWidth: 1,
            title: {
                text: metrics_displayName[0]
            },
            plotLines: [{
                color: 'black',
                dashStyle: 'dot',
                width: 2,
                label: {
                    rotation: 0,
                    y: 15,
                    style: {
                        fontStyle: 'italic'
                    },
                },
                zIndex: 3
            }]
        };
        this.chartOptions.yAxis={
            startOnTick: false,
            endOnTick: false,
            title: {
                text: metrics_displayName[1]
            },
            labels: {
                format: '{value}'
            },
            maxPadding: 0.2,
            plotLines: [{
                color: 'black',
                dashStyle: 'dot',
                width: 2,
                label: {
                    align: 'right',
                    style: {
                        fontStyle: 'italic'
                    },
                    x: -10
                },
                zIndex: 3
            }]
        }
        this.chartOptions.plotOptions.series = {
            dataLabels: {
                enabled: false,
                format: '{point.name}'
            }
        };

        this.chartOptions.showInLegend = true;
        this.chartOptions.series.type = 'bubble';
        this.chartOptions.xAxis.crosshair = false;

        const {widgetData} = widget;
        let metrics=_.pluck(widget.metrics, 'name');

        var series = [];
        const data1 = [], data2 = [];
        widgetData.result.map((item) => {
            var dimensionsRow;
            Object.keys(item.dimensions).map((key) => {
                dimensionsRow= item.dimensions[key];

            })
            const dateRow1 = item.dateRange1;
            const dateRow2 = item.dateRange2;
            data1.push({'x': parseFloat(dateRow1[metrics[0]].replace(/,/g , '')), 'y': parseFloat(dateRow1[metrics[1]].replace(/,/g , '')) ,'z': parseFloat(dateRow1[metrics[2]].replace(/,/g , '')),'dimension':dimensionsRow });
            data2.push({'x': parseFloat(dateRow2[metrics[0]].replace(/,/g , '')), 'y': parseFloat(dateRow2[metrics[1]].replace(/,/g , '')) ,'z': parseFloat(dateRow2[metrics[2]].replace(/,/g , '')),'dimension':dimensionsRow });
        });
        this.chartOptions.legend.adjustChartSize = true;
        this.chartOptions.legend.itemStyle = {
            font: '9pt Open Sans, arial, sans-serif',
            color: 'black',
            fontWeight: 'normal',
            //width: parseInt(strwidth) - 30 // or whatever
        }
        this.chartOptions.legend.symbolHeight= 8;
        this.chartOptions.legend.symbolWidth= 8;
        this.chartOptions.legend.symbolRadius= 6;

        this.chartOptions.series = [{
            name: legend+ "- "+ DateRange,
            data: data1
        },
            {
                name: legend+ "- "+ CompDateRange,
                data: data2
            }];
    }

    getOptions() {
        return this.chartOptions;
    }

    exportCSV(widget, filename) {
        if (!widget.isDateComparison) {
            this.exportCSVNormal(widget, filename);
        } else {
            this.exportCSVCompareWith(widget, filename)
        }
    }

    exportCSVNormal(widget, filename) {
        const {metrics, dimensions, widgetData} = widget;
        const metricsData = metrics.map((item) => {
            return {
                ...item,
            }
        })
        const columnMetadata = dimensions.concat(metricsData).map((item) => {
            return {
                columnName: item.name,
                displayName: item.displayName
            }
        })
        let header = {}, i = 0, j = 0;
        (columnMetadata).map((item, index) => {
            if (item.columns) {
                header[i++] = index === 0 ? "" : item.displayName;
            } else {
                header[i++] = item.displayName
            }
        })
        let csv = common.json2csv([header])
        csv += common.json2csv(widgetData.result)
        common.opencsv(csv, filename)
    }

    exportCSVCompareWith(widget, filename) {
        const {metrics, dimensions, widgetData} = widget;
        const dimensionFlags = {}, metricFlags = {}

        let dimensionDisplayName = dimensions.map((item, i) => {
            dimensionFlags[item.name] = i +2
            return item.displayName
        });
        let metricDisplayName = metrics.map((item, i) => {
            metricFlags[item.name] = i + dimensions.length +2
            return item.displayName
        })
        dimensionDisplayName.splice(0, 0, "Start Date");
        dimensionDisplayName.splice(1, 0, "End Date");

        let header = common.headerToCSV(dimensionDisplayName.concat(metricDisplayName));
        let csvdata ='';
        csvdata = csvdata + header + '\r\n';
        let array = [];
        let metricsName =  _.pluck(metrics, 'name');

        let fromTime = moment(parseInt(widget.fromTime)).format('MMM DD YYYY');
        let fromTime1 = moment(parseInt(widget.fromTime1)).format('MMM DD YYYY') ;
        let toTime = moment(parseInt(widget.toTime)).format('MMM DD YYYY');
        let toTime1 = moment(parseInt(widget.toTime1)).format('MMM DD YYYY') ;

        widgetData.result.map((item) => {
            const dateRow1 = item.dateRange1;
            const dateRow2 = item.dateRange2;
            var obj1 = new Array(metrics.length + dimensions.length + 2), obj2 = new Array(metrics.length + dimensions.length + 2);
            obj1[0] = fromTime; obj1[1] = toTime;
            obj2[0] = fromTime1; obj2[1] = toTime1;

            _.each(Object.keys(item.dimensions), function(value){
                let index = dimensionFlags[value];
                obj1[index] = item.dimensions[value];
                obj2[index] = item.dimensions[value];
            })

            _.each(metricsName, function (value) {
                let index = metricFlags[value];
                obj1[index] = dateRow1[value];
            })
            _.each(metricsName, function (value) {
                let index = metricFlags[value];
                obj2[index] = dateRow2[value];
            })
            array.push(obj1);
            array.push(obj2);
        });
        csvdata = csvdata + common.json2csv(array);
        common.opencsv(csvdata, filename)
    }
}

module.exports =  Bubble