/**
 * Created by 12072 on 8/9/16.
 */

var Chart =require('./Chart');
var _ =require('underscore');

class Gauge extends Chart {

    constructor(props) {
        super(props);

        let widget = props.widget;
        this.setCommonOptions()
        this.setOptions(widget);

        this.overrideChartOptionsByWidgetType(widget, props.barOptions);
    }

    overrideChartOptionsByWidgetType(bardata, barOptions = {}) {
        this.chartOptions = _.extend(
            this.chartOptions,
            barOptions
        )
    }

    setOptions(bardata) {
        if (!(typeof (bardata) == "undefined") || true) {
            this.chartOptions.chart.type = 'solidgauge';
            this.chartOptions.yAxis = {
                min: 0,
                max: 200,
                title: {
                    text: 'Speed'
                }
            }
            this.chartOptions.series = [{
                name: 'Speed',
                data: [Math.round(Math.random() * 100)],
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                        ( 'black') + '">{y}</span><br/>' +
                        '<span style="font-size:12px;color:silver">km/h</span></div>'
                },
                tooltip: {
                    valueSuffix: ' km/h'
                }
            }]
        }
    }

    setCommonOptions(){
        this.chartOptions.pane = {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor:'#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        }
        this.chartOptions.yAxis = {
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        }
        this.chartOptions.plotOptions = {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
        this.chartOptions.credits = {
            enabled: false
        }
    }

    labelFormatter(){
        var num = this.y;
        var isNegative = false;
        var formattedNumber;
        if (num < 0) {
            isNegative = true
        }
        num = Math.abs(num)
        if (num >= 1000000000) {
            formattedNumber = (num / 1000000000).toFixed(2).replace(/\.0$/, '') + 'G';
        } else if (num >= 1000000) {
            formattedNumber =  (num / 1000000).toFixed(2).replace(/\.0$/, '') + 'M';
        } else  if (num >= 1000) {
            formattedNumber =  (num / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
        } else {
            formattedNumber = num;
        }
        if(isNegative) { formattedNumber = '-' + formattedNumber }
        return formattedNumber;
    }

    getOptions() {
        return this.chartOptions;
    }
}

module.exports = Gauge