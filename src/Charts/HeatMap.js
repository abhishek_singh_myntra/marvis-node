/**
 * Created by 165322 on 11/11/16.
 */
var Chart =require('./Chart');
var _ = require('underscore');

class HeatMap extends Chart {

    constructor(props) {
        super(props);
        let widget = props.widget;
        if(widget.isDateComparison)
            this.setRangeOptions(widget);
        else
            this.setOptions(widget);

        this.overrideChartOptionsByWidgetType();
    }

    overrideChartOptionsByWidgetType() {
    }

    setOptions(heatmapdata) {
        var series = [];
        if (!(typeof (heatmapdata) == "undefined")) {
            let metrics=_.pluck(heatmapdata.metrics, 'name');
            let metrics_displayName=_.pluck(heatmapdata.metrics, 'displayName');
            var dimension_name=_.pluck(heatmapdata.dimensions, 'name')[0];
            var legend = _.pluck(heatmapdata.metrics, 'displayName').join(', ') + ' by ' + _.pluck(heatmapdata.dimensions, 'displayName').join(', ');
            this.chartOptions.chart.type = 'treemap';
            this.chartOptions.tooltip={
                pointFormat: '<span style="font-size:10px">  <b>{point.name}</b>'+
                '<br />'+ metrics_displayName[0] +': <b>{point.value}</b>' +
                '<br />'+ metrics_displayName[1] +': <b>{point.colorValue}</b>' ,
                shared: true,
                useHTML: true,
                headerFormat: `<span />`
            };
            this.chartOptions.title = {
                text: legend,
                align: 'center',
                verticalAlign: 'top',
                y: 0,
                style:{
                    fontSize: "1rem",
                    color:"#a09797"
                }
            };
            this.chartOptions.plotOptions.series = {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            };
            this.chartOptions.showInLegend = true;
            this.chartOptions.xAxis.crosshair = false;

            if (!(typeof (heatmapdata.widgetData.result) == 'undefined')) {
                _.each(heatmapdata.widgetData.result, (value) => {
                    if(parseFloat(value[metrics[1]].replace(/,/g , '')) <= 0){
                        value[metrics[1]]=(0.0001).toString();
                    }
                    series.push({ 'name': value[dimension_name], 'value': parseFloat(value[metrics[0]].replace(/,/g , '')),'colorValue': parseFloat(value[metrics[1]].replace(/,/g , ''))});
                });
            }
            else {
                series.push({'name': '', 'value': '', 'colorValue': ''});
            }
            this.chartOptions.colorAxis={
                type: 'logarithmic',
                minColor: '#EFEFFF',
                maxColor: '#000022',
                stops: [
                    [0, '#EFEFFF'],
                    [0.67, '#4444FF'],
                    [1, '#000022']
                ],
                gridLineColor: "white",
                showFirstLabel: false

            };
            this.chartOptions.legend.adjustChartSize = true;
            this.chartOptions.legend.itemStyle = {
                font: '9pt Open Sans, arial, sans-serif',
                color: 'black',
                fontWeight: 'normal',
                //width: parseInt(strwidth) - 30 // or whatever
            };
            this.chartOptions.series = [{
                turboThreshold: 10000,
                layoutAlgorithm: 'squarified',
                data: series,
                type: 'treemap'
            }];

        }
    }

    getOptions() {
        return this.chartOptions;
    }

    blendColors(c0, c1, p) {
        var f=parseInt(c0.slice(1),16);
        var t=parseInt(c1.slice(1),16);
        var R1=f>>16;
        var G1=f>>8&0x00FF;
        var B1=f&0x0000FF;
        var R2=t>>16;
        var G2=t>>8&0x00FF;
        var B2=t&0x0000FF;
        return "#"+(0x1000000+(Math.round((R2-R1)*p)+R1)*0x10000+(Math.round((G2-G1)*p)+G1)*0x100+(Math.round((B2-B1)*p)+B1)).toString(16).slice(1);
    }

    percentForColor(maxValue,relativeValue){
        if(relativeValue<0){
            relativeValue=0;
        }
        if(maxValue===0) {
            maxValue=maxValue+0.001;
        }
        var percent= ((100/maxValue)*relativeValue)/100;
        return percent;

    }
}

module.exports =  HeatMap