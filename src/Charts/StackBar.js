/**
 * Created by 12072 on 8/17/16.
 */
var Bar =require('./Bar');

class StackBar_ extends Bar {
    constructor(props) {
        super(props);
        this.chartOptions.plotOptions.series.stacking = 'normal';
    }

    getOptions() {
        return this.chartOptions;
    }
}

module.exports =  StackBar_
