/**
 * Created by 12072 on 04/03/17.
 */
export default {
    LINE: "Line",
    DRILL_DOWN_LINE: "DrillDownLine",
    PIE: "Pie",
    FLAT_BAR: "FlatBar",
    FLAT_BAR_VERTICAL: "FlatBarVertical",
    PERCENT_BAR: "PrecentBar",
    STACK_BAR: "StackBar",
    METRIC: "Metric",
    BUBBLE: "Bubble"
}