
var isBrowser = (typeof window !== 'undefined');

module.exports = {
  services: {
    nlp: 'http://marvis-test-service.mynt.myntra.com/marvis-api/rest/v1/nlp',
    udp: 'http://udp-fe.mynt.myntra.com/myntra/udp'
  }
};
